# Additions to run highrate files 
This directory provides the scripts and tool to upgrade a node to handle highrate files. This README explain how to upgrade a node, how to process the files and an example of the procedure.


## Author
Juliette Legrand (2024-06-21)

## Overview

0. Update the node configuration letter
1. Do the preliminary steps
2. Update the database to handle highrate files
3. install highrate_fwss, a tool dedicated to highrate files, in parallel of the current fwss
4. add a fistfull of highrate files with indexGD and highrate_fwss. Be careful to not target the current FWSS
5. check that the highrate files are correctly inserted in the database
6. Post upgrade steps

RunQC is **NOT** to be used on the highrate files, and for the moment there is no synchronization of the highrate metadata to the DGW, and no GLASS-API to get them from the database. 

For any doubt, please contact gnss-dgw@oca.eu and epos@oma.be

## Update the node configuration letter 
It is the first step, to be done before doing anything else. Update the configuration letter with the data center(s) for the high rate data and send it to gnss-dgw@oca.eu

Only when HR data centers are added at the DGW and synchronized to your node, can you do the following steps.

## Preliminary steps for upgrading the node to handle highrate files

1.  Stop the synchronization
2.  Stop glassfish
3.  Stop generating and inserting files and QC metadata in the node.
4.  backup the current database to a safe place

## Database upgrade
### Authors
Juliette Legrand 

### Requirements
Have the database v1-3-0 (03-05-2019) installed at the node. All current nodes have this version of the database.
<!-- if it is not the case, please contact gnss-dgw@oca.eu and epos@oma.be  -->


### Description
The current database is updated with a script to add and configure a table dedicated to highrate files.

1. to create the tables dedicated to highrate files in the current database version used at the node (EPOS Database v1-3-0 (03-05-2019)), 
run the script [database/Table_highrate_creation_20240525.sql](https://gitlab.com/gpseurope/highrate_fwss/-/blob/6ce45df007f2cd3470ef6f7b83567152339cfc56/database/Table_highrate_creation_20240525.sql)

2. check that the table `highrate_rinex_file` was added in the DB


### More details
This script is an addon to  the database EPOS Database v1-3-0 (03-05-2019) (https://gitlab.com/gpseurope/database/-/tree/c4d88f013cc0e4d8bee344b4b502cef9c2fe0f88)
to add the highrate part coming from database EPOS Database v3.0.3 (https://gitlab.com/gpseurope/database/-/tree/eee357bf339647ca8aa3affa462acfd42dcc955d) 

## highrate_fwss: a new tool dedicated to highrate files
### Authors
Daniele Randazzo, Patrizia Pizzulo and Juliette Legrand
### Description

This new tool is dedicated to only insert the highrate file metadata.
In practice, you will need to keep running your current FWSS for the daily files and this one (highrate_fwss) for the highrate files.

Both FWSS and highrate_fwss use the same principles and need to run on 2 different ports. The configuration file of highrate_fwss is also called `web_server.cfg`. The empty version of this file is called `web_server.cfg.template`, you have to copy it to `web_server.cfg` and configure the resulting file with the same parameters as the FWSS, but with a different port number.

The endpoint to be used for the highrate is: `/gps/data/rinexhighrate` , it is used exactly in the same way as `gps/data/rinex` and it is used the same way to insert the metadata computed by indexGD into the highrate table.

It is **crucial** to be careful to not mix up the two services, and to send the highrate files to highrate_fwss with the good port and good endpoint and the daily files to the usual FWSS.

## indexGD
The current version (1.2.7) of indexGD is able to generate metadata for highrate files. You can check the version by doing `python ./indexGeodeticData.py --version`. 

The usage is the same than for daily files, except for 3 crucial differences:

1. the parameters for the type of data show that they are highrate data: `-s 1h -f 1s`
2. the port is different
3. the endpoint is different: `/gps/data/rinexhighrate` instead of `/gps/data/rinex`

## Verify that the highrate files are correctly inserted in the database

The highrate files metadata are inserted in the table `highrate_rinex_file` in the database. You can check that the files are correctly inserted by doing a query on this table.

```sql
SELECT * FROM highrate_rinex_file;
```


## Post upgrade steps
1. start glassfish again
2. verify on GLASS-API and GUI that everything is working properly
3. contact gnss-dgw@oca.eu and epos@oma.be to inform that your node is hosting some highrate files
4. Once you have the greenlight, restart the synchronization and the generation of metadata for the daily and highrate files.

## QC, synchronization and GLASS-API
For the moment, there is no QC, no synchronization and no GLASS-API for the highrate files. 
QC will be implemented in a future version of indexGD. **DO NOT RUN RUNQC ON THE HIGHRATE FILES.**

Synchronization and GLASS API is under development.


## Usage example
Considering that the database has been updated, and highrate_fwss is installed, the following is an example showing how insert high rate files. 

The path to the high rate RINEX files to be processed are stored in a text file called filelisthighrate.lst.

For High Rate RINEX, the command line would be:
```bash
python ./indexGeodeticData.py  -l filelisthighrate.lst -t RINEX3 -s 1h -f 1s -r %YYYY%/%DOY% -d "data center name" -w http://dev-epos.oma.be:5556/gps/data/rinexhighrate --log_name filelisthighrate.json
```

Note that the parameters `-s 1h -f 1s` are used to specify that the data is highrate, that the port is 5556 and the endpoint is `/gps/data/rinexhighrate` because it targets highrate_fwss.

As a reminder, the command line for Daily RINEX would be:

```bash
./indexGeodeticData.py  -l filelistdaily.lst -t RINEX3 -s 24h -f 30s -r highrate/%YYYY%/%DOY% -d "data center name" -w http://dev-epos.oma.be:5555/gps/data/rinex --log_name filelistdaily.json
```

Where the lst of daily RINEX files are stored in the text file filedaily.lst, and the current fwss runs on the port 5555.  Note also the difference in the endpoint for the highrate files that is `gps/data/rinexhighrate` instead of `gps/data/rinex` for the daily RINEX.


## Other packages
- GLASS API version handling highrate is under testing (status 2024-06-21)
- synchronization is being develloped by DGW and will not use trigger anymore
