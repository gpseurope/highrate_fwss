# -*- coding: utf-8 -*-
# Responsible:    Daniele Randazzo, Patrizia Pizzulo
# Contributors: Juliette Legrand, Jean-Luc Menut
# Previous contributors: Fjalar Sigurðarson (fjalar@vedur.is),  Tim Sonnemann (tim@vedur.is)
# Date:   2024-06-20

from __init__ import *


# local app imports:
import queries
from queries import profile
# from queries_rnx import Queries_rnx
from queries_hr import Queries_hr
# from queries_qc import Queries_qc
###


FWSS_VERSION = "v1.0.0" # dr>20/06/2024 --> START HIGH RATE

app = Flask(__name__)
CORS(app)

def parseConfig(type =""):
    '''
    Config parser that returns necessary parameters for the server to run
    '''

    try:
        configFile = "web_server.cfg"
#        parser = SafeConfigParser()
        parser = ConfigParser()

        # Check if config file exists
        if os.path.isfile(configFile):

            parser.read(configFile)
            #configs.append(parser)

            ## Server parameters ##
            if type == "server":
                # app_debug = parser.get("Server", "app_debug")
                app_debug = parser.getboolean("Server", "app_debug") # gunicorn
                try:
                    app_run = parser.get("Server", "app_run")
                    app_port = int(parser.get("Server", "port"))
                except Exception as e:
                    app_run = None
                    app_port = 5000
                try:
                    app_proxy = parser.getboolean("Server", "proxy")
                except Exception as e:
                    app_proxy = False

                # return app_run, app_debug, app_port
                return app_run, app_debug, app_port, app_proxy

            ## Log rotating parameters ##
            if type == "log-rotation":
                # Pull log rotating settings info from the config file,
                # store in a dict and return it
                try:
                    maxBytes = int(parser.get("Log-rotation", "maxBytes"))
                except Exception as e:
                    maxBytes = 1000000

                if maxBytes <= 0:
                    maxBytes = 1000000

                try:
                    backupCount = int(parser.get("Log-rotation", "backupCount"))
                except Exception as e:
                    backupCount = 10

                if backupCount <= 0:
                    backupCount = 1

                log_info = {'maxBytes': maxBytes,
                            'backupCount': backupCount
                            }
                return log_info

            ## Database parameteres ##
            if type == "database":

                # Pull database settings info from the config file,
                # store in a dict and return it
                db_info = {'db_type':parser.get("Database", "db_type"),
                           'username':parser.get("Database", "username"),
                           'password':parser.get("Database", "password"),
                           'hostname':parser.get("Database", "hostname"),
                           'db_port': int(parser.get("Database", "db_port")),
                           'db_name':parser.get("Database", "db_name")
                           }

                return db_info

            if type == 'glass-api':
                # Pull GLASS-API info from the config file
                # store in a dict and return it


                glass_api_info = {'url': parser.get("GLASS-API", "url")}
                return glass_api_info

            if type == "benchmark":
                try:
                    active = parser.get("Benchmark", "active")
                    if active.lower() == 'true':
                        active = True
                    else:
                        active = False
                except Exception as e:
                    active = False

                try:
                    threshold_seconds = float(parser.get("Benchmark", "threshold_seconds"))
                except Exception as e:
                    threshold_seconds = 0.5

                benchmark = {
                    'active': active,
                    'threshold_seconds': threshold_seconds
                }
                return benchmark

        else:
            print('''>> Error in parseConfig(): Config file "{0}" does not exist.'''.format(configFile))
            print('''>> Please provide a valid config file. See default file "web_server.cfg.default"''')
            print('''>> rename it to web_server.cfg and configure for your environment.''')
            sys.exit(0)

    except Exception as e:
        #        print >> sys.stderr, "Error in parseConfig(): "
        #        print >> sys.stderr, e
        print(f"Error in parseConfig(): {e}")



# Change app middleware if it runs behind a reverse proxy.
app_run, app_debug, app_port, app_proxy = parseConfig("server")
if app_proxy:
    from werkzeug.middleware.proxy_fix import ProxyFix
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1, x_port=1)


## Instantiate a database conneciton
db_info = parseConfig("database")
log_info = parseConfig("log-rotation")

benchmark = parseConfig("benchmark")
queries.__benchmark__ = benchmark
if benchmark['active']:
    queries.create_benchmark_log(log_info)

queries = queries.Queries(db_info, log_info)
# queries_rnx = Queries_rnx(db_info, log_info)
queries_hr = Queries_hr(db_info, log_info)
# queries_qc = Queries_qc(db_info, log_info)

# load GLASS-API info from the config file
glass_api_info = parseConfig('glass-api')

###
### Services begin ###
###

@app.route('/', alias=True)
@app.route('/gps/', alias=True)
@app.route('/gps')
def hello():
    """
    Basic greeting message on server main page.
    """
    title = 'GNSS Europe'
    head1 = ('Hello! I am your friendly Flask webserver '
             'built to function as a GPS Web Service')
    rooturl = request.url_root
    services = [
        {'endpoint':'checkversion',
         'description':'Return running DB-API version'
        },

        {'endpoint': '/gps/data/rinexhighrate',
         'description': 'Index new High_Rate_RINEX file or update if exists. File metadata are provided in json format'
         },

    ]

    mailtocontact=None
    return render_template('services_home.html', # should be in 'templates/'
                           title=title,
                           head1=head1,
                           rooturl=rooturl,
                           services=services,
                           mailtocontact=mailtocontact)


@app.route('/gps/geodetic.yaml')
def send_yaml():
    return send_from_directory('swagger', 'geodetic.yaml')



#--- added by Daniele > 18/04/2024
@app.route('/gps/data/rinexhighrate', methods=['POST'])
def data_rinexhighrate_post():
    '''Data - Index/Post rinex data'''

    ## Formalize JSON body
    body = request.get_json(force=True)
#todo duplicate the function and add singularly the sub
    ## Post data to database
    query_result, status_code = queries_hr.data_rinexhighrate_post(body)

    # Process and deliver the responce
    result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    #result[0].headers['Content-type'] = 'application/json'

    return result
#---



@app.route('/checkversion', methods=['GET'])
def checkversion():

    # Process the response
    data = {}
    data['status'] = 200
    data['FWSS HIGH RATE version'] = FWSS_VERSION
    json_data = json.dumps(data)
    result = (Response(json_data, content_type='application/json'), 200)

    return result





if __name__ == "__main__":
    # Run this version of app.run while devloping. Only localhost/127.0.0.1 can access the server
    #app.run()

    # Run this version of app.run to expose the server to the internal network
    # app_run, app_debug, app_port = parseConfig("server")
    #app.run(host=app_run, debug=False, port=app_port)

    # The app is started by the Flask development server.
    app.run(host=app_run, debug=app_debug, port=app_port)
else:
    # The app is started by Gunicorn: use Gunicorn logger.
    import logging
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)