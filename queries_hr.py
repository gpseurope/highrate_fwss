from __init__ import *
from queries import Queries, GenericException


class Queries_hr(Queries):
    def __init__(self, db_info, log_info):  # Add arg3 for child-specific initialization
        super().__init__(db_info, log_info)  # Call parent class constructor with required arguments


    def data_rinexhighrate_post(self, body):
        # INGV 18/04/2024
        self.logger.info("data_rinexhighrate_post() was called")
        # rinex_file = RinexFile()

        rinex_file_id = None
        file_type_id = None
        data_center_id = None
        station_id = None

        status_code = NO_CONTENT
        status = "No status yet"

        if body['sampling_window'] not in ['1h', '01h']:
            status_code = CONFLICT
            status = f"Sampling window= {body['sampling_window']}, not High Rate (1h)"
            return status, status_code


        self.logger.info("Variables have been preset...")

        # Check mandatory fields
        status_ar = []
        for key in (
                "file_name",
                "relative_path",
                "reference_date",
                "station_marker",
                "data_center",
                "file_type",
                "sampling_window",
                "sampling_frequency",
                "md5_checksum",
                "revision_date",
                "creation_date",
        ):
            if key not in list(body.keys()) or not body[key]:
                # Define return status message
                status_ar.append(f"{key}")

            if status_ar:
                status = f"Missing body mandatory keys: {', '.join(status_ar)}"
                self.logger.error(status)
                self.logger.error(body)
                # Update status code
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

        if body["sampling_window"] in ("01D", "01d", "1D", "1d", "24H"):
            body["sampling_window"] = "24h"
            self.logger.info(
                f"""[{body['file_name']}] SAMPLING_WINDOW RECEIVED id [{body['sampling_window']}] HAS BEEN CONVERTED TO [24h]"""
            )

        self.logger.info(
            f"[{body['file_name']}] JSON body looks like this: {body}"
        )
        self.rinex_filename = body["file_name"]
        # retrieve needed parameters to update or reset the rinex_file table: station_id, datacenter_id, file_type
        with closing(self.session()) as session:
            try:
                station_id = (
                    session.query(Station.id)
                    .filter(Station.marker == body["station_marker"])
                    .scalar()
                )
            except MultipleResultsFound:
                status = f"""[{body['file_name']}]. Found multiple records with the same marker: [{body['station_marker']}]."""
                self.logger.error(status)
                status_code = CONFLICT
                self.rinex_filename = None
                return status, status_code

            if not station_id:
                # Define return status message
                status = f"""[{body['file_name']}] Station with marker "{body['station_marker']}" was not found. 
                    Station must be registered in the database before files can be indexed."""
                self.logger.error(status)
                # Update status code
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

            # there is a station with this station_marker in the DB, so ... continue
            data_center_id = (
                session.query(DataCenter.id)
                .filter(DataCenter.acronym == body["data_center"])
                .scalar()
            )

            if not data_center_id:
                status = f"""[{body['file_name']}] Data center with acronym: "{body['data_center']}" not in database.
                    Please contact the DGW, the data_center information should be inserted by the DGW."""
                self.logger.error(status)
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

            # We have a data center registered, so ... still continue
            # data_center_id = data_center_id[0]
            self.logger.info(
                f"""[{body["file_name"]}]  Data center with acronym: "{body["data_center"]}" found with ID: "{data_center_id}" """
            )

            # Check if the file type exists. If not, create from the one given in the JSON body.

            file_type_id = (
                session.query(FileType.id)
                .filter(
                    and_(
                        FileType.format == body["file_type"],
                        FileType.sampling_window == body["sampling_window"],
                        FileType.sampling_frequency == body["sampling_frequency"],
                    )
                )
                .scalar()  # replaced instead of .one_or_none()
            )

            if not file_type_id:
                status = f"""[{body['file_name']}] File_type "{body['file_type']}" not in database.
                    Please contact the DGW, the file_type information should be inserted by the DGW."""
                self.logger.error(status)
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

            data_center_structure_id = (
                session.query(DataCenterStructure.id)
                .filter(DataCenterStructure.id_data_center == data_center_id)
                .filter(DataCenterStructure.id_file_type == file_type_id)
                .scalar()
            )
            if not data_center_structure_id:
                status = f"""[{body['file_name']}] data_center_structure not in database:
                data_center: {body["data_center"]}
                file_type:   {body["file_type"]}
                window:      {body["sampling_window"]}
                frequency:   {body["sampling_frequency"]}
                Please contact the DGW, the data_center_structure information should be inserted by the DGW."""
                self.logger.error(status)
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

            # We have a data center registered, so ... still continue
            self.logger.info(
                f"""[{body['file_name']}] found in DB with Data center: "{body['data_center']}" ID: {data_center_id} - File_type "{body['file_type']}"."""
            )

            # ---

            #   1^ case: query rinex_file with the same md5
            try:
                rinex_file = (
                    session.query(HighrateRinexFile, DataCenterStructure)
                    .join(DataCenterStructure)
                    .filter(HighrateRinexFile.md5checksum == body["md5_checksum"])
                    .one_or_none()
                )
            except MultipleResultsFound:
                status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" result duplicated inside the database"""
                self.logger.error(status)
                status_code = CONFLICT
                self.rinex_filename = None
                return status, status_code

            if rinex_file:
                # 1^case condition: from query(RinexFile, DataCenterStructure), with same md5
                # check if the rinex_file (identified by name, type and data_center) is unique
                if (
                        session.query(HighrateRinexFile)
                                .join(DataCenterStructure)
                                .filter(
                            and_(
                                HighrateRinexFile.name == body["file_name"],
                                HighrateRinexFile.id_file_type == file_type_id,
                                DataCenterStructure.id_data_center == data_center_id,
                                HighrateRinexFile.md5checksum != body["md5_checksum"],
                            )
                        )
                                .count() > 0
                ):  # TESTED
                    status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" 
                        file_name+file_type+data_center must be unique. You are trying to set it to {body['file_name']} - {body['sampling_window']} - {body['sampling_frequency']}, but these values already exist for file with another md5checksum"""
                    self.logger.error(status)
                    status_code = CONFLICT
                    self.rinex_filename = None
                    return status, status_code

                # initialize status
                #                status = f"""[{body['file_name']}] Rinex file was found by id: {rinex_file.RinexFile.id}.
                #                Nothing seems changed. File has not been updated!"""
                status_code = NOT_MODIFIED

                # ---
                # cases which require the reset of rinex_file
                force_reset_update_rinex = False
                if station_id != rinex_file.HighrateRinexFile.id_station:
                    status = f"""Station_Id is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                if file_type_id != rinex_file.HighrateRinexFile.id_file_type:
                    status = f"""File_type_id is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                try:
                    ref_date = datetime.strptime(body["reference_date"], "%Y-%m-%d")  # Rinex2 return only date
                except Exception:
                    try:
                        ref_date = datetime.strptime(body["reference_date"],
                                                     "%Y-%m-%d %H:%M:%S")  # Rinex3 return date+time
                    except Exception:
                        status = f"""[{body['file_name']}] error on "reference_date": {body["reference_date"]}, exit"""
                        self.logger.error(status)
                        status_code = CONFLICT
                        self.rinex_filename = None
                        return status, status_code

                if (ref_date != rinex_file.HighrateRinexFile.reference_date):
                    status = f"""Reference_date is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                # --- check dates if creation and/or revision date are NULL or if different from the body
                # and initializes the rinex_file.creation_date and rinex_file.revision_date
                # which are not changed by reset or update
                force_update_rinex = self.check_creation_revision_date_highrate( #todo duplicate for HighRate_check...
                    body, rinex_file.HighrateRinexFile
                )  # TESTED

                if force_reset_update_rinex:
                    status, status_code = self.reset_rinex_file_highrate(
                        body,
                        session,
                        rinex_file.HighrateRinexFile,
                        station_id,
                        data_center_structure_id,
                        file_type_id,
                    )
                    self.rinex_filename = None
                    return status, status_code

                if body["file_name"] != rinex_file.HighrateRinexFile.name:
                    status = f"""[{body['file_name']}] Setting new file_name from body, found [{rinex_file.RinexFile.name}] in DB"""
                    self.logger.info(status)
                    force_update_rinex = True

                if (
                        data_center_id != rinex_file.DataCenterStructure.id_data_center
                ):
                    status = f"""[{body['file_name']}] Setting new data_center from body, found [{body['data_center']}] in body"""
                    try:
                        data_center_structure_id = (
                            session.query(DataCenterStructure.id)
                            .join(DataCenter)
                            .filter(
                                and_(
                                    (DataCenter.id == data_center_id),
                                    (DataCenterStructure.id_file_type == file_type_id),
                                )
                            )
                            .scalar()
                        )
                    except MultipleResultsFound as e:
                        status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" DataCenterStructure multiple entry"""
                        self.logger.error(status)
                        status_code = CONFLICT
                        self.rinex_filename = None
                        return status, status_code
                    self.logger.info(status)
                    force_update_rinex = True

                if (
                        body["relative_path"] != rinex_file.HighrateRinexFile.relative_path
                ):  # TESTED
                    status = f"""[{body['file_name']}] Setting relative_path from body, found [{body['relative_path']}] in body"""
                    self.logger.info(status)
                    force_update_rinex = True

                # if one of them has changed, then just update, but keep status  and don't delete file
                if force_update_rinex:
                    self.logger.info(
                        f"""[{body['file_name']}] Updating rinex_file information"""
                    )
                    self.update_rinex_file_highrate(
                        body,
#                        session,
                        rinex_file.HighrateRinexFile,
                        station_id,
                        data_center_structure_id,
                        file_type_id,
                        rinex_file.HighrateRinexFile.status,
                    )
                    session.flush()
                    session.commit()
                    status_code = ACCEPTED  # INDICATES TO NOT RUN THE QC

            else:  # 2^ case: rinex_file w/ DIFFERENT MD5, retrieve by file_name
                try:
                    rinex_file = (
                        session.query(HighrateRinexFile)
                        .join(DataCenterStructure)
                        .filter(
                            and_(
                                HighrateRinexFile.name == body["file_name"],
                                HighrateRinexFile.id_file_type == file_type_id,
                                DataCenterStructure.id_data_center == data_center_id,
                            )
                        )
                        .one_or_none()
                    )
                except MultipleResultsFound:
                    status = f"""More then one rinex file with name: [{body['file_name']}] type: "{body['file_type']}
                        +{body['sampling_window']}+{body['sampling_frequency']}" and data center: {body['data_center']} exist on DB ..."""
                    self.logger.error(status)
                    status_code = CONFLICT
                    self.rinex_filename = None
                    return status, status_code

                if rinex_file:
                    # --- check dates if creation and/or revision date are NULL or if different from the body
                    # and initialize the rinex_file.creation_date and rinex_file.revision_date
                    force_update_rinex = self.check_creation_revision_date_highrate(
                        body, rinex_file
                    )
                    status, status_code = self.reset_rinex_file_highrate(
                        body,
                        session,
                        rinex_file,
                        station_id,
                        data_center_structure_id,
                        file_type_id,
                    )


                else:  # 3^ case: create a new db entry for rinex file
                    self.logger.info(
                        f"""[{body['file_name']}] Inserting new rinex file record in the database"""
                    )
                    status, status_code = self.insert_highrate_rinex_file(
                        body, session, station_id, data_center_structure_id, file_type_id
                    )

        if status_code == 304:
            status = f"""[{body['file_name']}] HighRate_Rinex file was found by id: {rinex_file.HighrateRinexFile.id}. 
                Nothing seems changed. File has not been updated!"""
            self.logger.info(status)

        self.rinex_filename = None
        return status, status_code


#----

    def check_creation_revision_date_highrate(self, body, rinex_file):
        force_update_rinex = False
        if type(rinex_file.revision_date) is datetime:
            revision_date_db = rinex_file.revision_date
        else:
            revision_date_db = None
            self.logger.info("revision_date from db is NULL")

        if type(rinex_file.creation_date) is datetime:
            creation_date_db = rinex_file.creation_date
        else:
            creation_date_db = None
            self.logger.info("creation_date from db is NULL")

        # Grouped NULL cases for revision_date and creation_date
        # 2024-02-01 JLe: only revision_date from IndexGD should be used as creation_date is not reliable and depends on the system and will be removed from IndexGD output in the future
        if (revision_date_db == None) and (creation_date_db == None):
            rinex_file.creation_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
            rinex_file.revision_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
            status = f"""Setting creation_date and revision_date from the json: [{body['revision_date']}, {body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        elif (revision_date_db != None) and (creation_date_db == None):
            rinex_file.creation_date = revision_date_db  # TESTED!!!
            rinex_file.revision_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
            status = f"""Setting creation_date from revision_date db [{revision_date_db}], and revision_date from json [{body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        elif (revision_date_db == None) and (creation_date_db != None):
            rinex_file.revision_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
            status = f"""Setting revision_date from json [{body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        else:
            if (datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S") != revision_date_db):
                rinex_file.revision_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
                status = (f"""Updating revision_date from json [{body['revision_date']}]""")
                self.logger.info(status)
                force_update_rinex = True

        # From MinuteMeeting_20240125,
        # change the fwss, if creation_date > revision_date then
        # creation_date = min(old_revision_date, modification_date)
        # revision_date = modification_date

        if (rinex_file.creation_date > rinex_file.revision_date):

            if revision_date_db is None:
                rinex_file.creation_date = datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S")
            else:
                rinex_file.creation_date = min(revision_date_db,
                                               datetime.strptime(body["revision_date"], "%Y-%m-%d %H:%M:%S"))
            status = (
                f"creation_date > revision_date, updating creation_date with the min (old revision_date and new revision_date )): {rinex_file.creation_date}")
            self.logger.info(status)
            force_update_rinex = True

        return force_update_rinex



#----
    def insert_highrate_rinex_file(
        self, body, session, station_id, data_center_structure_id, file_type_id
    ):
        try:
            rinex_file = HighrateRinexFile()
            # rinex_file.creation_date = body["creation_date"]  #   08/02/2024 never use the body["creation_date"], only refer to revision_date
            rinex_file.creation_date = body["revision_date"]
            rinex_file.revision_date = body["revision_date"]

            self.update_rinex_file_highrate(
                body, rinex_file, station_id, data_center_structure_id, file_type_id, 0
            )
            session.add(rinex_file)
            self.logger.info(f"""[{body['file_name']} rinex_file id: {rinex_file.id}""")
            session.flush()
            session.commit()
            status_code = CREATED
            status = f"""[{body['file_name']}] Rinex file has been indexed with id {rinex_file.id}"""
            self.logger.info(status)
        except Exception as e:
            session.rollback()
            status_code = INTERNAL_SERVER_ERROR
            myname = sys._getframe().f_code.co_name
            status = f"[{body['file_name']}] Unknown error seems to have occurred in {myname}, error: {str(e)}"
            self.logger.error(status)

        return status, status_code

#--
    # create a rinex_file datamodel object
    def update_rinex_file_highrate(
        self,
        body,
#        session,
        rinex_file,
        station_id,
        data_center_structure_id,
        file_type_id,
        status,
    ):
        try:
            #### Insert Rinex file info ####
            self.logger.info(
                f"""[{body['file_name']}] Updating rinex_file information"""
            )

            ### Fill None not present key or empty in json input ###
            # INGV March 2023
            if "file_size" in body and body["file_size"]:
                rinex_file_size = body["file_size"]
            else:
                rinex_file_size = None

            if "md5uncompressed" in body.keys() and body["md5uncompressed"]:
                rinex_file_md5uncompressed = body["md5uncompressed"]
            else:
                rinex_file_md5uncompressed = None


            rinex_file.name = body["file_name"]  # Mandatory
            rinex_file.id_station = station_id
            rinex_file.id_data_center_structure = data_center_structure_id
            rinex_file.file_size = rinex_file_size
            rinex_file.id_file_type = file_type_id
            rinex_file.relative_path = body["relative_path"]  # Mandatory
            rinex_file.reference_date = body["reference_date"]  # Mandatory
            rinex_file.published_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #            the creation_date and revision_date are only set by the check_creation_revision_date fuction
            #            rinex_file.creation_date = body['creation_date']
            #            rinex_file.revision_date = body['revision_date']
            rinex_file.md5checksum = body["md5_checksum"]  # Mandatory
            rinex_file.md5uncompressed = rinex_file_md5uncompressed
            rinex_file.status = status

        except Exception as e:
            myname = sys._getframe().f_code.co_name
            errMsg = f"""[{body['file_name']}] Error in function {myname}. error message: {str(e)} """
            self.logger.error(errMsg)
            raise GenericException(errMsg)

#---

    # -------------------------

    def reset_rinex_file_highrate(
        self, body, session, rinex_file, station_id, data_center_structure_id, file_type_id
    ):
        # reset => update to the body parameters, set the status=0 and delete_QC
        try:
            self.update_rinex_file_highrate(
                body, rinex_file, station_id, data_center_structure_id, file_type_id, 0
            )
#todo check on highrate there's no QC, removed the following "delete_QcReportSummary" - DR > 17/05/2024
            # self.delete_QcReportSummary(session, rinex_file.id)
            session.flush()
            session.commit()

            status = f"""[{body['file_name']}] changed in station_id or file_type or reference_date. Going to reset rinex_file"""
            self.logger.info(status)
            status_code = OK

        except Exception as e:
            session.rollback()
            status_code = INTERNAL_SERVER_ERROR
            myname = sys._getframe().f_code.co_name
            status = f"""[{body['file_name']}] Error occurred in reset_rinex_file_highrate().id {rinex_file.id}, 
            Error in function {myname}. Error message: {str(e)}"""
            self.logger.error(status)

        return status, status_code

