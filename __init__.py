## General imports
import sys
import json
import os.path
from configparser import ConfigParser
#from httplib import (

import requests
#from urlparse import urljoin
from urllib.parse import urljoin

# third party imports:
from flask import (
    Flask,
    request,
    render_template,
    Response,
    send_from_directory,
)
from flask_cors import CORS

import re
import sys
import json
import logging
import logging.handlers
import decimal
from contextlib import closing
from datetime import datetime

# from httplib import NO_CONTENT, OK, NOT_FOUND, CONFLICT, BAD_REQUEST, INTERNAL_SERVER_ERROR, CREATED
from http.client import (
    NO_CONTENT,
    OK,
    NOT_FOUND,
    CONFLICT,
    BAD_REQUEST,
    INTERNAL_SERVER_ERROR,
    CREATED,
    ACCEPTED,
    NOT_MODIFIED,
)

## SqlAlchemy imports
from sqlalchemy import (
    create_engine,
    or_,
    and_,
    func,
    desc)

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound

from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import text
import os
import xmltodict
import inspect




# 09/06/2023 better format with datamodel.Node
## Data model imports

from datamodel_gnss_hybrid_v1_v3 import (
Agency,
Attribute,
Bedrock,
Constellation,
Coordinates,
Country,
DocumentType,
Effects,
FileType,
GnssObsnames,
HighrateRinexErrorTypes,
ItemType,
LogType,
Monument,
Node,
QcParameters,
RinexErrorTypes,
Service,
StationType,
t_stations_with_files_summary,
t_stations_with_highrate_files_summary,
Tectonic,
UserGroup,
Contact,
DataCenter,
FailedQueries,
Geological,
ItemTypeAttribute,
State,
City,
DataCenterStructure,
Item,
Network,
NodeDataCenter,
ItemAttribute,
Location,
Station,
Condition,
Connections,
DatacenterStation,
Document,
FileGenerated,
HighrateRinexFile,
InstrumentCollocation,
LocalTies,
Log,
OtherFiles,
Queries,
RinexFile,
StationColocation,
StationContact,
StationItem,
StationNetwork,
UserGroupStation,
ColocationOffset,
HighrateReportSummary,
HighrateRinexErrors,
QcReportSummary,
RinexErrors,
QcConstellationSummary,
QcNavigationMsg,
QcObservationSummaryC,
QcObservationSummaryD,
QcObservationSummaryL,
QcObservationSummaryS,
)

# from datamodel import (
#     Station,
#     Contact,
#     StationContact,
#     StationNetwork,
#     StationColocation,
#     Connection,
#     Document,
#     EstimatedCoordinate,
#     OtherFile,
#     Jump,
#     Noise,
#     ReferencePositionVelocity,
#     Log,
#     UserGroupStation,
#     SeasonalSignal,
#     Query,
#     FileGenerated,
#     Location,
#     Agency,
#     City,
#     State,
#     Country,
#     Network,
#     Bedrock,
#     Geological,
#     Tectonic,
#     StationType,
#     Coordinates,
#     Monument,
#     LocalTie,
#     InstrumentColocation,
#     ItemType,
#     Attribute,
#     RinexFile,
#     FileType,
#     DataCenter,
#     StationItem,
#     Item,
#     ItemAttribute,
#     ReceiverType,
#     AntennaType,
#     RadomeType,
#     DataCenterStructure,
#     QualityFile,
#     QcParameter,
#     QcConstellationSummary,
#     QcNavigationMsg,
#     QcObservationSummaryC,
#     QcObservationSummaryD,
#     QcObservationSummaryL,
#     QcObservationSummaryS,
#     QcReportSummary,
#     Constellation,
#     GnssObsname,
#     RinexErrorType,
#     RinexError,
#     Node,
#     Log,
#     LogType,
#     Effect,
#     Condition,
#     DatacenterStation,
# )




# XML parser for QC files:
# from parse_qc_xml import parse_qcxml

# __SB__
from sqlalchemy.dialects import postgresql

import string
from random import *

"""
    Stuff needed for benchmarking
"""
from functools import wraps
import time


#  THIS PART IS SPECIFIC FOR BENCHMARK LOG
# =======================================================
__benchmark__ = {"active": False, "threshold_seconds": 0.5}
benchmark_logger = None
PROF_DATA = {}

def profile(func):
    global __benchmark__

    @wraps(func)
    def with_profiling(*args, **kwargs):
        start_time = time.time()

        ret = func(*args, **kwargs)

        if __benchmark__["active"]:
            elapsed_time = time.time() - start_time

            if func.__name__ not in PROF_DATA:
                PROF_DATA[func.__name__] = {
                    "called_times": 0,
                    "min_time": 999999999,
                    "max_time": 0,
                    "avg_time": 0,
                }
            if elapsed_time < PROF_DATA[func.__name__]["min_time"]:
                PROF_DATA[func.__name__]["min_time"] = elapsed_time
            if elapsed_time > PROF_DATA[func.__name__]["max_time"]:
                PROF_DATA[func.__name__]["max_time"] = elapsed_time
            PROF_DATA[func.__name__]["avg_time"] = (
                PROF_DATA[func.__name__]["avg_time"]
                * PROF_DATA[func.__name__]["called_times"]
                + elapsed_time
            ) // (
                PROF_DATA[func.__name__]["called_times"] + 1
            )
            PROF_DATA[func.__name__]["called_times"] += 1

            if elapsed_time >= __benchmark__["threshold_seconds"]:
                msg = (
                    "Function %s called %d times. Execution time last: %.3f,  min: %.3f, max: %.3f, average: %.3f"
                    % (
                        func.__name__,
                        PROF_DATA[func.__name__]["called_times"],
                        elapsed_time,
                        PROF_DATA[func.__name__]["min_time"],
                        PROF_DATA[func.__name__]["max_time"],
                        PROF_DATA[func.__name__]["avg_time"],
                    )
                )
                benchmark_logger.info(msg)
        return ret

    return with_profiling

def clear_prof_data():
    global PROF_DATA
    PROF_DATA = {}

"""
     prepare the logger for benchmark messages
"""

def create_benchmark_log(log_info):
    global benchmark_logger
    benchmark_logger = logging.getLogger("BC_" + __name__)
    benchmark_logger.setLevel(logging.DEBUG)

    log_dir = "log"
    if not os.path.exists(log_dir):
        os.makedirs(log_dir, 0o755)

    bc_log_file_name = os.path.join(log_dir, "benchmark_queries.log")
    bc_f_handler = logging.handlers.RotatingFileHandler(
        bc_log_file_name,
        maxBytes=log_info["maxBytes"],
        backupCount=log_info["backupCount"],
    )
    bc_f_handler.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    bc_formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s: %(message)s", datefmt="%m/%d/%y %H:%M"
    )

    # create formatter and add it to the handlers
    bc_f_handler.setFormatter(bc_formatter)

    # add the handlers to the logger
    benchmark_logger.addHandler(bc_f_handler)

# =======================================================


def row2dict(row):
    """
    Convert sqlalchemy query output (1 table object) into dict.
    This avoids having to deal with 'column.__dict__', which includes an extra
    field '_sa_instance_state' and may or may not get more in future releases.
    """
    d = {}
    for column in row.__table__.columns:
        d[column.name] = getattr(row, column.name)
    return d

def querylist2dictlist(query_list):
    """
    Convert sqlalchemy query output (list of tuples of table objects) into
    a list of dicts.
    This is convenient if multiple (n>1) results for multiple output tables
    are given by session.query().
    The output of this method will have the structure:
        out_list[0][table_name][column_name]
        ...
        out_list[n-1][table_name][column_name]
    """
    out_list = []
    for query_tuple in query_list:
        mod_dict = {}
        # Convert each table object (1 row) to dict:
        for q_obj in query_tuple:
            mod_dict[q_obj.__tablename__] = row2dict(q_obj)
        out_list.append(mod_dict)
    return out_list
