--
-- Name: highrate_rinex_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.highrate_rinex_file (
    id integer NOT NULL,
    name text NOT NULL,
    id_station integer NOT NULL,
    id_data_center_structure integer NOT NULL,
    file_size integer,
    id_file_type integer NOT NULL,
    relative_path text NOT NULL,
    reference_date timestamp without time zone NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    revision_date timestamp without time zone NOT NULL,
    md5checksum text NOT NULL,
    md5uncompressed text NOT NULL,
    status smallint NOT NULL,
    rinex_version text,
    qc_publish_date timestamp without time zone,
    t3_timestamp timestamp without time zone
);


--
-- Name: stations_with_highrate_files_summary; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.stations_with_highrate_files_summary AS
 SELECT s.marker,
    min(f.creation_date) AS start_date,
    max(f.creation_date) AS end_date,
    count(f.name) AS number_files_stored,
    sum(f.file_size) AS total_files_size
   FROM (public.station s
     JOIN public.highrate_rinex_file f ON ((s.id = f.id_station)))
  GROUP BY s.id, s.marker
  ORDER BY (count(f.name)) DESC
  WITH NO DATA;

--
-- Name: highrate_rinex_file pk_rinex_file_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT pk_rinex_file_highrate PRIMARY KEY (id);


--
-- Name: highrate_rinex_file uq_rinex_file_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT uq_rinex_file_highrate UNIQUE (md5checksum);

--
-- Name: highrate_rinex_file fk_rinex_file_highrate_data_center_structure; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_data_center_structure FOREIGN KEY (id_data_center_structure) REFERENCES public.data_center_structure(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_file fk_rinex_file_highrate_file_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_file_type FOREIGN KEY (id_file_type) REFERENCES public.file_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_file fk_rinex_file_highrate_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;






--
-- Name: highrate_rinex_file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.highrate_rinex_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: highrate_rinex_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.highrate_rinex_file_id_seq OWNED BY public.highrate_rinex_file.id;

ALTER TABLE ONLY public.highrate_rinex_file ALTER COLUMN id SET DEFAULT nextval('public.highrate_rinex_file_id_seq'::regclass);