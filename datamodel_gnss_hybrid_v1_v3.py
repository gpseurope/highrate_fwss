# from sqlalchemy import BigInteger, Boolean, CHAR, Column, Date, DateTime, Enum, Float, ForeignKeyConstraint, Index, Integer, Numeric, PrimaryKeyConstraint, SmallInteger, String, Table, Text, UniqueConstraint, text
# from sqlalchemy.dialects.postgresql import TIMESTAMP
# from sqlalchemy.orm import declarative_base, relationship

## SqlAlchemy imports
from sqlalchemy import (
    BigInteger, Boolean, Column, Date, DateTime, Enum, Float,
    ForeignKey, Index, Integer, Numeric, SmallInteger, String,
    Text, UniqueConstraint, text,
    PrimaryKeyConstraint, CHAR, Table, ForeignKeyConstraint, TIMESTAMP
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base



Base = declarative_base()
metadata = Base.metadata


class Agency(Base):
    __tablename__ = 'agency'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_agency'),
        UniqueConstraint('abbreviation', name='uq_agency_abbreviation'),
        {'comment': 'Operational Center.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(Text, nullable=False, comment='Institutional name of the operational center.')
    abbreviation = Column(String(50), server_default=text('NULL::character varying'), comment='Unique acronym associated to the operational center and used for its identification.')
    address = Column(Text, server_default=text('NULL::character varying'), comment='Institutional address.')
    www = Column(String(250), server_default=text('NULL::character varying'), comment='Institutional website url.')
    infos = Column(Text, comment='Additional information to characterize the operational center.')

    contact = relationship('Contact', back_populates='agency')
    data_center = relationship('DataCenter', back_populates='agency')


class Attribute(Base):
    __tablename__ = 'attribute'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_attributes'),
        UniqueConstraint('name', name='uq_attributes_name'),
        {'comment': "Specification of an item's attributes (e.g., radome, antenna, "
                'filter).'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Attribute name.')

    item_type_attribute = relationship('ItemTypeAttribute', back_populates='attribute')
    item_attribute = relationship('ItemAttribute', back_populates='attribute')


class Bedrock(Base):
    __tablename__ = 'bedrock'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_bedrock'),
        {'comment': 'Specification of type and condition of bedrock.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    condition = Column(Text, nullable=False, comment='Characterization of bedrock condition.')
    type = Column(Text, comment='Specification of bedrock type.')

    geological = relationship('Geological', back_populates='bedrock')


class Constellation(Base):
    __tablename__ = 'constellation'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_constellation'),
        {'comment': 'List of known satellite navigation systems (constellations).'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    identifier_1ch = Column(String, nullable=False, comment='Satellite system identifier: 1-char id.')
    identifier_3ch = Column(String(3), nullable=False, comment='Satellite system identifier: 3-char id.')
    name = Column(String(127), nullable=False, comment='Full acronym of satellite system.')
    official = Column(Boolean, nullable=False, comment='Whether this constellation is officially recognized by the DB admin.')

    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='constellation')
    qc_navigation_msg = relationship('QcNavigationMsg', back_populates='constellation')


class Coordinates(Base):
    __tablename__ = 'coordinates'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_coordinates'),
        {'comment': 'List of station coordinates in (x, y, z) and (lat, long, alt) '
                'formats.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    x = Column(Numeric, nullable=False, comment='X coordinate.')
    y = Column(Numeric, nullable=False, comment='Y coordinate.')
    z = Column(Numeric, nullable=False, comment='X coordinate.')
    lat = Column(Numeric(6, 4), comment='Latitude in decimal degrees.')
    lon = Column(Numeric(7, 4), comment='Longitude in decimal degrees.')
    altitude = Column(Numeric(6, 2), comment='Altitude in meters.')

    location = relationship('Location', back_populates='coordinates')


class Country(Base):
    __tablename__ = 'country'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_country'),
        {'comment': 'List of world countries by name and ISO 3166-1 alpha-3 codes.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(Text, nullable=False, comment='Country name.')
    iso_code = Column(Text, nullable=False, comment='Country ISO 3166-1 alpha-3 code.')

    state = relationship('State', back_populates='country')


class DocumentType(Base):
    __tablename__ = 'document_type'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_document_types'),
        {'comment': 'List of document types.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Type of document.')

    document = relationship('Document', back_populates='document_type')


class Effects(Base):
    __tablename__ = 'effects'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_effects'),
        {'comment': 'List of effects that characterize a station condition.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    type = Column(Text, nullable=False, comment='Type of effect.')

    condition = relationship('Condition', back_populates='effects')


class FileType(Base):
    __tablename__ = 'file_type'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_file_type'),
        {'comment': 'Types of files acceptable as data files.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    format = Column(Text, nullable=False, comment='Acceptable formats are: RINEX2, RINEX3, RINEXN2, RINEXN3, RINEXM2, RINEXM3, QC-XML.')
    sampling_window = Column(Text, comment='Time window of data collection: 24hour, 1hour, 15min.')
    sampling_frequency = Column(Text, comment='Time between datapoints: 30s, 15s, 1s, 5Hz, 10Hz.')

    highrate_rinex_file = relationship('HighrateRinexFile', back_populates='file_type')
    rinex_file = relationship('RinexFile', back_populates='file_type')


class GnssObsnames(Base):
    __tablename__ = 'gnss_obsnames'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_gnss_obsnames'),
        {'comment': 'List of all the possible observations in a GNSS RINEX file.'}
    )

    id = Column(SmallInteger, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(3), nullable=False, comment='Observation name with 3 characters (as defined in RINEX 3.03 convention), or 2 char + 1 space if RINEX 2 input.')
    frequency_band = Column(Integer, nullable=False, comment='Frequency-band id number as in RINEX 3.')
    obstype = Column(CHAR(1), nullable=False, comment='Type of observation:  C  for code (also  P  in RINEX 2),  L  for phase,  S  for signal to noise ratio, and  D  for doppler.')
    official = Column(Boolean, nullable=False, comment='Whether officially recognized combination in RINEX 2 and 3 definitions.')
    channel = Column(CHAR(1), comment='Tracking channel.')

    qc_observation_summary_c = relationship('QcObservationSummaryC', back_populates='gnss_obsnames')
    qc_observation_summary_d = relationship('QcObservationSummaryD', back_populates='gnss_obsnames')
    qc_observation_summary_l = relationship('QcObservationSummaryL', back_populates='gnss_obsnames')
    qc_observation_summary_s = relationship('QcObservationSummaryS', back_populates='gnss_obsnames')


class HighrateRinexErrorTypes(Base):
    __tablename__ = 'highrate_rinex_error_types'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_rinex_types_highrate'),
    )

    id = Column(Integer, primary_key=True)
    error_type = Column(Text)

    highrate_rinex_errors = relationship('HighrateRinexErrors', back_populates='highrate_rinex_error_types')


class ItemType(Base):
    __tablename__ = 'item_type'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_item_types'),
        {'comment': 'List of different item types.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Item name.')

    item_type_attribute = relationship('ItemTypeAttribute', back_populates='item_type')
    item = relationship('Item', back_populates='item_type')


class LogType(Base):
    __tablename__ = 'log_type'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_log_types'),
        UniqueConstraint('name', name='uq_log_types_name'),
        {'comment': '~List of log types.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(30), nullable=False, comment='Type log name.')

    log = relationship('Log', back_populates='log_type')


class Monument(Base):
    __tablename__ = 'monument'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_monument'),
        {'comment': 'List of relevant information to characterize survey monuments.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    description = Column(Text, nullable=False, comment='Monument descrition.')
    inscription = Column(Text, comment='Monument inscription.')
    height = Column(Numeric, comment='Monument height.')
    foundation = Column(Text, comment='Monument foundation.')
    foundation_depth = Column(Numeric, comment='Monument foundation depth.')

    station = relationship('Station', back_populates='monument')


class Node(Base):
    __tablename__ = 'node'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_node'),
        {'comment': 'List of existing nodes.'}
    )

    name = Column(String(30), nullable=False, comment='Node name.')
    host = Column(String(30), nullable=False, comment='Host in which the node was deployed.')
    port = Column(String(30), nullable=False, comment='Node port.')
    dbname = Column(String(30), nullable=False, comment='Name of the database deployed at the node.')
    username = Column(String(30), nullable=False, comment='Node database username.')
    password = Column(String(64), nullable=False, comment='Node database password.')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    url = Column(String(100), comment='Node url.')
    email = Column(String(150), comment='Node contact person email.')
    contact_name = Column(String(100), comment='Node contact person name.')

    failed_queries = relationship('FailedQueries', back_populates='node')
    node_data_center = relationship('NodeDataCenter', back_populates='node')
    connections = relationship('Connections', foreign_keys='[Connections.destiny]', back_populates='node')
    connections_ = relationship('Connections', foreign_keys='[Connections.source]', back_populates='node_')


class QcParameters(Base):
    __tablename__ = 'qc_parameters'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_qc_parameters'),
        {'comment': 'Store the parameters with which the QC has been done.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    software_name = Column(Text, nullable=False, comment='Name of the QC analysis software used.')
    software_version = Column(Text, nullable=False, comment='QC analysis software version number.')
    elevation_cutoff = Column(Integer, nullable=False, comment='User defined elevation cut off.')
    gps = Column(Boolean, nullable=False, comment='True if GPS is expected.')
    glo = Column(Boolean, nullable=False, comment='True if GLONASS is expected.')
    gal = Column(Boolean, nullable=False, comment='True if GALILEO is expected.')
    bds = Column(Boolean, nullable=False, comment='True if BEIDOU is expected.')
    qzs = Column(Boolean, nullable=False, comment='True if QZSS is expected.')
    sbs = Column(Boolean, nullable=False, comment='True if SBAS is expected.')
    nav_gps = Column(Boolean, nullable=False, comment='True if there is GPS in the navigation file used.')
    nav_glo = Column(Boolean, nullable=False, comment='True if there is GLONASS in the navigation file used.')
    nav_gal = Column(Boolean, nullable=False, comment='True if there is GALILEO in the navigation file used.')
    nav_bds = Column(Boolean, nullable=False, comment='True if there is BEIDOU in the navigation file used.')
    nav_qzs = Column(Boolean, nullable=False, comment='True if there is QZSS in the navigation file used.')
    nav_sbs = Column(Boolean, nullable=False, comment='True if there is SBAS in the navigation file used.')
    nav_irn = Column(Boolean, nullable=False, comment='True if there is IRNSS in the navigation file used.')
    irn = Column(Boolean, comment='True if IRNSS is expected.')
    fwss_version = Column(Text, comment='True if there is IRNSS in the navigation file used.')

    qc_report_summary = relationship('QcReportSummary', back_populates='qc_parameters')


class RinexErrorTypes(Base):
    __tablename__ = 'rinex_error_types'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_rinex_error_types'),
        UniqueConstraint('error_type', name='uq_rinex_error_types_error_type'),
        {'comment': 'List of rinex files errors.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    error_type = Column(Text, comment='Reference (foreign key) from error_type table.')

    rinex_errors = relationship('RinexErrors', back_populates='rinex_error_types')


class Service(Base):
    __tablename__ = 'service'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_service'),
        {'comment': 'Table used to validate which service version should be allowed to '
                'access data from the current database'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key')
    name = Column(String(100), nullable=False, comment='Name of the service to be validated')
    version = Column(Numeric(3, 2), nullable=False, comment='The internal version of the service')
    createdon = Column(DateTime, nullable=False, server_default=text('CURRENT_TIMESTAMP'), comment='Service creation date')
    comments = Column(Text, server_default=text('NULL::character varying'), comment='Additional detail on service')
    updatedon = Column(DateTime, comment='Service update date')


class StationType(Base):
    __tablename__ = 'station_type'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_station_types'),
        UniqueConstraint('name', name='uq_location_types_name'),
        {'comment': 'List of station types.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Station name.')
    type = Column(String(50), comment='Station type.')

    station = relationship('Station', back_populates='station_type')


t_stations_with_files_summary = Table(
    'stations_with_files_summary', metadata,
    Column('marker', String(4)),
    Column('start_date', DateTime),
    Column('end_date', DateTime),
    Column('number_files_stored', BigInteger),
    Column('total_files_size', BigInteger)
)


t_stations_with_highrate_files_summary = Table(
    'stations_with_highrate_files_summary', metadata,
    Column('marker', String(4)),
    Column('start_date', DateTime),
    Column('end_date', DateTime),
    Column('number_files_stored', BigInteger),
    Column('total_files_size', BigInteger)
)


class Tectonic(Base):
    __tablename__ = 'tectonic'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_tectonic'),
        {'comment': 'List of tectonic information relevant to stations.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    plate_name = Column(Text, nullable=False, comment='Tectonic plate information.')

    location = relationship('Location', back_populates='tectonic')


class UserGroup(Base):
    __tablename__ = 'user_group'
    __table_args__ = (
        PrimaryKeyConstraint('id', name='pk_user_group'),
        {'comment': 'Access control user groups consistent with the authenticating '
                'LDAP-server'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Name of user_group.')

    user_group_station = relationship('UserGroupStation', back_populates='user_group')


class Contact(Base):
    __tablename__ = 'contact'
    __table_args__ = (
        ForeignKeyConstraint(['id_agency'], ['agency.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_agency_id'),
        PrimaryKeyConstraint('id', name='pk_contacts'),
        UniqueConstraint('name', 'email', name='uq_contacts_namemail'),
        {'comment': 'List of contact persons for agency, station, network, log and '
                'items.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(50), nullable=False, comment='Contact person name.')
    id_agency = Column(Integer, nullable=False, comment='Reference (foreign key) to agency table to identify contact person agency.')
    title = Column(String(50), comment='Contact person title.')
    email = Column(String(400), comment='Contact person institutional email.')
    phone = Column(String(50), comment='Contact person institutional phone number.')
    gsm = Column(String(50), comment='Contact person cellular phone number.')
    comment = Column(Text, comment='Additional information on contact person.')
    role = Column(String(50), comment='Contact person specific role.')
    fax = Column(String(50), comment='Contact person institutional fax number.')

    agency = relationship('Agency', back_populates='contact')
    item = relationship('Item', foreign_keys='[Item.id_contact_as_owner]', back_populates='contact')
    item_ = relationship('Item', foreign_keys='[Item.id_contact_as_producer]', back_populates='contact_')
    network = relationship('Network', back_populates='contact')
    log = relationship('Log', back_populates='contact')
    station_contact = relationship('StationContact', back_populates='contact')


class DataCenter(Base):
    __tablename__ = 'data_center'
    __table_args__ = (
        ForeignKeyConstraint(['id_agency'], ['agency.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_data_center_agency'),
        PrimaryKeyConstraint('id', name='pk_data_center'),
        UniqueConstraint('acronym', name='uq_data_center_acronym'),
        Index('idx_data_center', 'id_agency'),
        {'comment': 'Table to store Data center information.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    acronym = Column(String(100), nullable=False, comment='Unique acronym used to identify data center.')
    hostname = Column(String(200), nullable=False, comment='Full DNS name of the computer serving the file (e.g.,  ftp.example.com).')
    root_path = Column(Text, nullable=False, comment='The relative path to the data directory from the server root, do not start with slash (/).')
    name = Column(String(100), nullable=False, comment='Data center name.')
    protocol = Column(String(5), nullable=False, comment='Communication protocol, most commonly ftp, http, sftp, https.')
    id_agency = Column(Integer, comment='Reference (foreign_key) to agency table.')

    agency = relationship('Agency', back_populates='data_center')
    data_center_structure = relationship('DataCenterStructure', back_populates='data_center')
    node_data_center = relationship('NodeDataCenter', foreign_keys='[NodeDataCenter.id_data_center]', back_populates='data_center')
    node_data_center_ = relationship('NodeDataCenter', foreign_keys='[NodeDataCenter.id_data_center]', back_populates='data_center_')
    datacenter_station = relationship('DatacenterStation', back_populates='data_center')


class FailedQueries(Base):
    __tablename__ = 'failed_queries'
    __table_args__ = (
        ForeignKeyConstraint(['destiny'], ['node.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_failed_queries_node'),
        PrimaryKeyConstraint('id', name='pk_failed_queries'),
        Index('idx_failed_queries', 'destiny'),
        {'comment': 'Failed_queries performed.'}
    )

    query = Column(Text, nullable=False, comment='Failed query.')
    destiny = Column(Integer, nullable=False, comment='Reference (foreign key) from node table that query failed.')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    reason = Column(Text, nullable=False, comment='Failed query description.')
    timestamp = Column(DateTime, server_default=text('now()'), comment='Timestamp format (YYYY-MM-DD hh:mm:ss).')

    node = relationship('Node', back_populates='failed_queries')


class Geological(Base):
    __tablename__ = 'geological'
    __table_args__ = (
        ForeignKeyConstraint(['id_bedrock'], ['bedrock.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_bedrock'),
        PrimaryKeyConstraint('id', name='pk_geological'),
        {'comment': 'Geological characterization including reference to bedrock type '
                'and condition.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    characteristic = Column(Text, nullable=False, comment='Geological characterization.')
    id_bedrock = Column(Integer, comment='Reference (foreign key) from bedrock table.')
    fracture_spacing = Column(Text, comment='Fracture spacing information.')
    fault_zone = Column(Text, comment='Fault zone information.')
    distance_to_fault = Column(Text, comment='Distance to the fault information.')

    bedrock = relationship('Bedrock', back_populates='geological')
    station = relationship('Station', back_populates='geological')


class ItemTypeAttribute(Base):
    __tablename__ = 'item_type_attribute'
    __table_args__ = (
        ForeignKeyConstraint(['id_attribute'], ['attribute.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_item_type_attribute_attribute'),
        ForeignKeyConstraint(['id_item_type'], ['item_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_item_type_attribute_item_type'),
        PrimaryKeyConstraint('id', name='pk_item_type_attribute'),
        Index('idx_item_type_attribute', 'id_item_type'),
        Index('idx_item_type_attribute_0', 'id_attribute'),
        {'comment': 'List of attributes of a specific item type.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_item_type = Column(Integer, nullable=False, comment='Reference (foreign key) from item_type table.')
    id_attribute = Column(Integer, nullable=False, comment='Reference (foreign key) from attribute table.')

    attribute = relationship('Attribute', back_populates='item_type_attribute')
    item_type = relationship('ItemType', back_populates='item_type_attribute')


class State(Base):
    __tablename__ = 'state'
    __table_args__ = (
        ForeignKeyConstraint(['id_country'], ['country.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_country_id'),
        PrimaryKeyConstraint('id', name='pk_state'),
        {'comment': 'List of administrative regions in which the station is located '
                '(state, province, county).'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_country = Column(Integer, nullable=False, comment='Reference (foreign key) from country table.')
    name = Column(Text, nullable=False, comment='Name of the administrative region (state,province,county).')

    country = relationship('Country', back_populates='state')
    city = relationship('City', back_populates='state')


class City(Base):
    __tablename__ = 'city'
    __table_args__ = (
        ForeignKeyConstraint(['id_state'], ['state.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_state_id'),
        PrimaryKeyConstraint('id', name='pk_city'),
        {'comment': 'Location name (place, city, town), where the station is '
                'installed.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_state = Column(Integer, nullable=False, comment='Reference (foreign key) of the administrative region in which the station is located (state, province,  county).')
    name = Column(Text, nullable=False, comment='Name of the location in which the station is located (place, town, city).')

    state = relationship('State', back_populates='city')
    location = relationship('Location', back_populates='city')


class DataCenterStructure(Base):
    __tablename__ = 'data_center_structure'
    __table_args__ = (
        ForeignKeyConstraint(['id_data_center'], ['data_center.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_dc_structure_data_center'),
        PrimaryKeyConstraint('id', name='pk_dc_structure'),
        Index('idx_dc_structure', 'id_data_center'),
        {'comment': 'Describe the repository structure depending on the file type.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_data_center = Column(Integer, nullable=False, comment='Reference (foreign key) to data_center table.')
    id_file_type = Column(Integer, nullable=False, comment='Type of file.')
    directory_naming = Column(Text, nullable=False, comment='Give the directory naming schema with the usual  WWWW  for GPS week,  DDD  for day and so on.')
    comments = Column(Text, comment='Additional information.')

    data_center = relationship('DataCenter', back_populates='data_center_structure')
    highrate_rinex_file = relationship('HighrateRinexFile', back_populates='data_center_structure')
    other_files = relationship('OtherFiles', back_populates='data_center_structure')
    rinex_file = relationship('RinexFile', back_populates='data_center_structure')


class Item(Base):
    __tablename__ = 'item'
    __table_args__ = (
        ForeignKeyConstraint(['id_contact_as_owner'], ['contact.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_contacts_as_owner_items'),
        ForeignKeyConstraint(['id_contact_as_producer'], ['contact.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_contacts_as_producer_items'),
        ForeignKeyConstraint(['id_item_type'], ['item_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_item_types_items'),
        PrimaryKeyConstraint('id', name='pk_items'),
        {'comment': 'Characterization of an item type, owner and producer.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_item_type = Column(Integer, nullable=False, comment='Reference (foreign key) from item_type table to specify te type of item.')
    id_contact_as_producer = Column(Integer, comment='Reference (foreign key) from contact table to identify the item producer contact.')
    id_contact_as_owner = Column(Integer, comment='Reference (foreign key) from contact table to identify the item owner contact.')
    comment = Column(Text, comment='Additional information.')

    contact = relationship('Contact', foreign_keys=[id_contact_as_owner], back_populates='item')
    contact_ = relationship('Contact', foreign_keys=[id_contact_as_producer], back_populates='item_')
    item_type = relationship('ItemType', back_populates='item')
    item_attribute = relationship('ItemAttribute', back_populates='item')
    document = relationship('Document', back_populates='item')
    station_item = relationship('StationItem', back_populates='item')


class Network(Base):
    __tablename__ = 'network'
    __table_args__ = (
        ForeignKeyConstraint(['id_contact'], ['contact.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_contacts_networks'),
        PrimaryKeyConstraint('id', name='pk_networks'),
        UniqueConstraint('name', name='uq_networks_name'),
        {'comment': 'List of network names and associated contact person responsible '
                'for the network.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(100), nullable=False, comment='Unique network name.')
    id_contact = Column(Integer, comment='Reference (foreign key) from contact table.')

    contact = relationship('Contact', back_populates='network')
    station_network = relationship('StationNetwork', back_populates='network')


class NodeDataCenter(Base):
    __tablename__ = 'node_data_center'
    __table_args__ = (
        ForeignKeyConstraint(['id_data_center'], ['data_center.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_node_data_center_data_center'),
        ForeignKeyConstraint(['id_data_center'], ['data_center.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_node_data_center'),
        ForeignKeyConstraint(['id_node'], ['node.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_node_data_center_node'),
        PrimaryKeyConstraint('id', name='pk_node_data_center'),
        UniqueConstraint('id_node', 'id_data_center', name='uq_node_data_center'),
        Index('idx_node_data_center_data_center', 'id_data_center'),
        Index('idx_node_data_center_id_data_center', 'id_data_center'),
        Index('idx_node_data_center_id_node', 'id_node'),
        Index('idx_node_data_center_node', 'id_node'),
        {'comment': 'List to show in which nodes a data_center for a station would be '
                'deployed.'}
    )

    id_node = Column(Integer, nullable=False, comment='Reference (foreign_key) from node table.')
    id_data_center = Column(Integer, nullable=False, comment='Reference (foreign_key) from data_center table.')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')

    data_center = relationship('DataCenter', foreign_keys=[id_data_center], back_populates='node_data_center')
    data_center_ = relationship('DataCenter', foreign_keys=[id_data_center], back_populates='node_data_center_')
    node = relationship('Node', back_populates='node_data_center')


class ItemAttribute(Base):
    __tablename__ = 'item_attribute'
    __table_args__ = (
        ForeignKeyConstraint(['id_attribute'], ['attribute.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_attribute_item_attribute'),
        ForeignKeyConstraint(['id_item'], ['item.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_item_item_attribute'),
        PrimaryKeyConstraint('id', name='pk_item_attribute'),
        {'comment': 'List all attributes of an item.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_item = Column(Integer, nullable=False, comment='Reference (foreign key) from item table item.')
    id_attribute = Column(Integer, nullable=False, comment='Reference (foreign key) from item table attribute.')
    date_from = Column(TIMESTAMP(), server_default=text("('now'::text)::timestamp without time zone"), comment='Start date in timestamp format (YYYY-MM-DD hh:mm:ss).')
#    date_from = Column(TIMESTAMP(precision=6), server_default=text("('now'::text)::timestamp without time zone"), comment='Start date in timestamp format (YYYY-MM-DD hh:mm:ss).')
    date_to = Column(TIMESTAMP(), comment='End date in timestamp format (YYYY-MM-DD hh:mm:ss).')
#    date_to = Column(TIMESTAMP(precision=6), comment='End date in timestamp format (YYYY-MM-DD hh:mm:ss).')
    value_varchar = Column(String(50), comment='Maximum value (50 characters).')
    value_date = Column(Date, comment='Date format (YYYY-MM-DD).')
    value_numeric = Column(Numeric, comment='Numeric value.')

    attribute = relationship('Attribute', back_populates='item_attribute')
    item = relationship('Item', back_populates='item_attribute')


class Location(Base):
    __tablename__ = 'location'
    __table_args__ = (
        ForeignKeyConstraint(['id_city'], ['city.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_city'),
        ForeignKeyConstraint(['id_coordinates'], ['coordinates.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_coordinates'),
        ForeignKeyConstraint(['id_tectonic'], ['tectonic.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_tectonic'),
        PrimaryKeyConstraint('id', name='pk_location'),
        {'comment': 'Characterization of the station location, including related '
                'information in other tables like coordinates, tectonic, city, '
                'state and country.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_city = Column(Integer, nullable=False, comment='Reference (foreign key) from city table.')
    id_coordinates = Column(Integer, nullable=False, comment='Reference (foreign key) from coordinates table.')
    id_tectonic = Column(Integer, comment='Reference (foreign key) from tectonic table.')
    description = Column(Text, comment='Addition description to characterize the station location.')

    city = relationship('City', back_populates='location')
    coordinates = relationship('Coordinates', back_populates='location')
    tectonic = relationship('Tectonic', back_populates='location')
    station = relationship('Station', back_populates='location')


class Station(Base):
    __tablename__ = 'station'
    __table_args__ = (
        ForeignKeyConstraint(['id_geological'], ['geological.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_geological'),
        ForeignKeyConstraint(['id_location'], ['location.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_location'),
        ForeignKeyConstraint(['id_monument'], ['monument.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_monument'),
        ForeignKeyConstraint(['id_station_type'], ['station_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_types_stations'),
        PrimaryKeyConstraint('id', name='pk_stations'),
        UniqueConstraint('marker', 'monument_num', 'receiver_num', 'country_code', name='uq_markerlongname'),
        {'comment': 'Set of characteristics of a station and related to additional '
                'information provided by related tables.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(100), nullable=False, comment='Station name.')
    marker = Column(String(4), nullable=False, comment='Unique four character identification marker.')
    description = Column(Text, comment='Additional information.')
    date_from = Column(DateTime, comment='Date the station was installed in timestamp format (YYYY-MM-DD hh:mm:ss).')
    date_to = Column(DateTime, comment='Date the station was removed in timestamp format (YYYY-MM-DD hh:mm:ss).')
    id_station_type = Column(Integer, comment='Type of station.')
    comment = Column(Text, comment='Additional information.')
    id_location = Column(Integer, comment='Reference (foreign key) from location table.')
    id_monument = Column(Integer, comment='Reference (foreign key) from monument table.')
    id_geological = Column(Integer, comment='Reference (foreign key) from geological table.')
    iers_domes = Column(String(9), server_default=text('NULL::character varying'), comment='Unique IERS Domes number.')
    cpd_num = Column(String(15), server_default=text('NULL::character varying'), comment='Station Cape Photographic Durchmusterung number (cpd_num).')
    monument_num = Column(Integer, comment='Number to identify station monument following RINEX 3 convention.')
    receiver_num = Column(Integer, comment='Number to identify station receiver following RINEX 3 convention.')
    country_code = Column(String(3), comment='Country code (ISO 3166-1 alpha-3 code).')

    geological = relationship('Geological', back_populates='station')
    location = relationship('Location', back_populates='station')
    monument = relationship('Monument', back_populates='station')
    station_type = relationship('StationType', back_populates='station')
    condition = relationship('Condition', back_populates='station')
    connections = relationship('Connections', back_populates='station_')
    datacenter_station = relationship('DatacenterStation', back_populates='station')
    document = relationship('Document', back_populates='station')
    file_generated = relationship('FileGenerated', back_populates='station')
    highrate_rinex_file = relationship('HighrateRinexFile', back_populates='station')
    instrument_collocation = relationship('InstrumentCollocation', back_populates='station')
    local_ties = relationship('LocalTies', back_populates='station')
    log = relationship('Log', back_populates='station')
    other_files = relationship('OtherFiles', back_populates='station')
    queries = relationship('Queries', back_populates='station')
    rinex_file = relationship('RinexFile', back_populates='station')
    station_colocation = relationship('StationColocation', foreign_keys='[StationColocation.id_station]', back_populates='station')
    station_colocation_ = relationship('StationColocation', foreign_keys='[StationColocation.id_station_colocated]', back_populates='station_')
    station_contact = relationship('StationContact', back_populates='station')
    station_item = relationship('StationItem', back_populates='station')
    station_network = relationship('StationNetwork', back_populates='station')
    user_group_station = relationship('UserGroupStation', back_populates='station')


class Condition(Base):
    __tablename__ = 'condition'
    __table_args__ = (
        ForeignKeyConstraint(['id_effect'], ['effects.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_effect'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station'),
        PrimaryKeyConstraint('id', name='pk_condition'),
        {'comment': 'Condition and effects applied to station.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from table station.')
    id_effect = Column(Integer, nullable=False, comment='Reference (foreign key) from table effects.')
    date_from = Column(DateTime, server_default=text("('now'::text)::timestamp without time zone"), comment='Start date of the specific condition.')
    date_to = Column(DateTime, comment='End date of the specific condition.')
    degradation = Column(Text, comment='Degradation characterization of thr condition.')
    comments = Column(Text, comment='Addtional information to characterize the condition.')

    effects = relationship('Effects', back_populates='condition')
    station = relationship('Station', back_populates='condition')


class Connections(Base):
    __tablename__ = 'connections'
    __table_args__ = (
        ForeignKeyConstraint(['destiny'], ['node.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_destiny_node'),
        ForeignKeyConstraint(['source'], ['node.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_source_node'),
        ForeignKeyConstraint(['station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_connections_station'),
        PrimaryKeyConstraint('id', name='pk_connections'),
        Index('idx_connections_destiny', 'destiny'),
        Index('idx_connections_source', 'source'),
        Index('idx_connections_station', 'station'),
        {'comment': 'Node connections performed for each station.'}
    )

    source = Column(Integer, nullable=False, comment='Reference (foreign key) to source node table.')
    destiny = Column(Integer, nullable=False, comment='Reference (foreign key) to destiny node table.')
    metadata_ = Column('metadata', String(2), nullable=False, comment='Metadata characterization information.')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    station = Column(Integer, comment='Reference (foreign key) to station table.')

    node = relationship('Node', foreign_keys=[destiny], back_populates='connections')
    node_ = relationship('Node', foreign_keys=[source], back_populates='connections_')
    station_ = relationship('Station', back_populates='connections')


class DatacenterStation(Base):
    __tablename__ = 'datacenter_station'
    __table_args__ = (
        ForeignKeyConstraint(['id_datacenter'], ['data_center.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_data_center_station'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_data_center_station_station'),
        PrimaryKeyConstraint('id', name='pk_datacenter_station'),
        Index('idx_data_center_station', 'id_station'),
        Index('idx_data_center_station_0', 'id_datacenter'),
        {'comment': 'Link between datacenter and station.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, comment='Reference (foreign key) to station table.')
    id_datacenter = Column(Integer, comment='Reference (foreign key) to data_center table.')
    datacenter_type = Column(Text, comment='Data center type: primary (defined by station owner via site log submission center), secondary (defined by station owner via site log submission center),  mirror (station owner say if he wants to have a mirror and data center confirm if they want to set it).')

    data_center = relationship('DataCenter', back_populates='datacenter_station')
    station = relationship('Station', back_populates='datacenter_station')


class Document(Base):
    __tablename__ = 'document'
    __table_args__ = (
        ForeignKeyConstraint(['id_document_type'], ['document_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_document_types_documents'),
        ForeignKeyConstraint(['id_item'], ['item.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_items_documents'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_stations_documents'),
        PrimaryKeyConstraint('id', name='pk_documents'),
        {'comment': 'Different types of documentation on stations and items.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    date = Column(Date, nullable=False, comment='Document date.')
    title = Column(Text, nullable=False, comment='Document title.')
    link = Column(String(60), nullable=False, comment='Document location (url).')
    id_document_type = Column(Integer, nullable=False, comment='Reference (foreign key) to item_type table to specify the document type.')
    description = Column(Text, comment='Document detailed description.')
    id_station = Column(Integer, comment='Reference (foreign key) to station table.')
    id_item = Column(Integer, comment='Reference (foreign key) to item table.')

    document_type = relationship('DocumentType', back_populates='document')
    item = relationship('Item', back_populates='document')
    station = relationship('Station', back_populates='document')


class FileGenerated(Base):
    __tablename__ = 'file_generated'
    __table_args__ = (
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station'),
        PrimaryKeyConstraint('id', name='pk_file_generated'),
        {'comment': 'List of types of files generated by a station.'}
    )

    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table).')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_file_type = Column(Integer, nullable=False, comment='Reference (foreign key) from file_type table.')

    station = relationship('Station', back_populates='file_generated')


class HighrateRinexFile(Base):
    __tablename__ = 'highrate_rinex_file'
    __table_args__ = (
        ForeignKeyConstraint(['id_data_center_structure'], ['data_center_structure.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_file_highrate_data_center_structure'),
        ForeignKeyConstraint(['id_file_type'], ['file_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_file_highrate_file_type'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_file_highrate_station'),
        PrimaryKeyConstraint('id', name='pk_rinex_file_highrate'),
        UniqueConstraint('md5checksum', name='uq_rinex_file_highrate')
    )

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    id_station = Column(Integer, nullable=False)
    id_data_center_structure = Column(Integer, nullable=False)
    id_file_type = Column(Integer, nullable=False)
    relative_path = Column(Text, nullable=False)
    reference_date = Column(DateTime, nullable=False)
    creation_date = Column(DateTime, nullable=False)
    published_date = Column(DateTime, nullable=False)
    revision_date = Column(DateTime, nullable=False)
    md5checksum = Column(Text, nullable=False)
    md5uncompressed = Column(Text, nullable=False)
    status = Column(SmallInteger, nullable=False)
    file_size = Column(Integer)
    rinex_version = Column(Text)
    qc_publish_date = Column(DateTime)
    t3_timestamp = Column(DateTime)

    data_center_structure = relationship('DataCenterStructure', back_populates='highrate_rinex_file')
    file_type = relationship('FileType', back_populates='highrate_rinex_file')
    station = relationship('Station', back_populates='highrate_rinex_file')
    highrate_report_summary = relationship('HighrateReportSummary', back_populates='highrate_rinex_file')
    highrate_rinex_errors = relationship('HighrateRinexErrors', back_populates='highrate_rinex_file')


class InstrumentCollocation(Base):
    __tablename__ = 'instrument_collocation'
    __table_args__ = (
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_collocation_instrument_stations'),
        PrimaryKeyConstraint('id', name='pk_collocation_instrument'),
        {'comment': 'Information on station instrument collocation.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from the station table.')
    type = Column(String(100), nullable=False, comment='Acceptable types are: GPS, GLONASS, DORIS, PRARE, SLR, VLBI, TIME, etc.')
    status = Column(Enum('PERMANENT', 'MOBILE', 'NONE', name='coloc_status'), comment='Acceptable values are: PERMANENT, MOBILE, NONE.')
    date_from = Column(DateTime, comment='Start date in timestamp format (YYYY-MM-DD hh:mm:ss).')
    date_to = Column(DateTime, comment='End date in timestamp format (YYYY-MM-DD hh:mm:ss).')
    comment = Column(Text, comment='Additional information.')

    station = relationship('Station', back_populates='instrument_collocation')


class LocalTies(Base):
    __tablename__ = 'local_ties'
    __table_args__ = (
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_local_ties_stations'),
        PrimaryKeyConstraint('id', name='pk_local_ties'),
        {'comment': 'Station local ties characterization.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table.')
    name = Column(String(100), nullable=False, comment='Local tie name.')
    usage = Column(String(100), comment='Local tie type of usage information.')
    cpd_num = Column(String(9), comment='Local tie Cape Photographic Durchmusterung number (cpd_num value).')
    iers_domes = Column(String(9), comment='Unique International Earth Rotation and Reference Systems Service (IERS) Dome number.')
    dx = Column(Numeric, comment='DX differential Components from GNSS Marker to the tied monument (ITRS) in (m).')
    dy = Column(Numeric, comment='DY differential Components from GNSS Marker to the tied monument (ITRS) in (m)')
    dz = Column(Numeric, comment='DZ differential Components from GNSS Marker to the tied monument (ITRS) in (m).')
    accuracy = Column(Numeric, comment='Accuracy values in millimeters.')
    survey_method = Column(String(100), comment='Survey method description.')
    date_at = Column(Date, comment='Date of measurement for the local tie in date format (YYYY-MM-DD).')
    comment = Column(Text, comment='Additional comments.')

    station = relationship('Station', back_populates='local_ties')


class Log(Base):
    __tablename__ = 'log'
    __table_args__ = (
        ForeignKeyConstraint(['id_contact'], ['contact.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_contact'),
        ForeignKeyConstraint(['id_log_type'], ['log_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_log_types_logs'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_stations_logs'),
        PrimaryKeyConstraint('id', name='pk_logs'),
        {'comment': 'Detail log information for a station including addtional '
                'information like contact person and log type from in obtained '
                'from tables contact and log_type.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    title = Column(String(50), nullable=False, comment='Log title name.')
    id_log_type = Column(Integer, nullable=False, comment='Reference (foreign key) from log_type table.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table.')
    date = Column(Date, comment='Log date format (YYYY-MM-DD).')
    modified = Column(Text, comment='Modified information.')
    previous = Column(String(30), comment='Previous site log file information.')
    id_contact = Column(Integer, comment='Person who has modified or created the site log.')

    contact = relationship('Contact', back_populates='log')
    log_type = relationship('LogType', back_populates='log')
    station = relationship('Station', back_populates='log')


class OtherFiles(Base):
    __tablename__ = 'other_files'
    __table_args__ = (
        ForeignKeyConstraint(['id_data_center_structure'], ['data_center_structure.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_other_files_data_center_structure'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_other_files_station'),
        PrimaryKeyConstraint('id', name='pk_files_1'),
        UniqueConstraint('md5checksum', name='uq_other_files_md5checksum'),
        Index('idx_file_7', 'id_data_center_structure'),
        Index('idx_other_files', 'id_station'),
        {'comment': 'List of other data files.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(String(250), nullable=False, comment='File name.')
    id_data_center_structure = Column(Integer, nullable=False, comment='Reference (foreign key) from data_center_structure table.')
    id_file_type = Column(Integer, nullable=False, comment='Reference (foreign key) from file_type table.')
    relative_path = Column(Text, nullable=False, comment='File relative path.')
    status = Column(Integer, nullable=False, comment='File current status.')
    file_size = Column(Integer, comment='File size.')
    creation_date = Column(Date, comment='Date format (YYYY-MM-DD).')
    revision_time = Column(DateTime, comment='Revision time timestamp format (YYYY-MM-DD hh:mm:ss).')
    md5checksum = Column(Text, comment='File unique md5checksum value.')
    id_station = Column(Integer, comment='Reference (foreign key) from station table.')

    data_center_structure = relationship('DataCenterStructure', back_populates='other_files')
    station = relationship('Station', back_populates='other_files')


class Queries(Base):
    __tablename__ = 'queries'
    __table_args__ = (
        ForeignKeyConstraint(['station_id'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_queries_station'),
        PrimaryKeyConstraint('id', name='pk_queries'),
        Index('idx_queries', 'station_id'),
        {'comment': 'Queries performed.'}
    )

    query = Column(Text, nullable=False, comment='Query performed.')
    metadata_ = Column('metadata', String(2), nullable=False, comment='Metadata information.')
    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    station_id = Column(Integer, comment='Reference (foreign key) from station table.')
    destiny = Column(Integer, comment='Node destiny.')

    station = relationship('Station', back_populates='queries')


class RinexFile(Base):
    __tablename__ = 'rinex_file'
    __table_args__ = (
        ForeignKeyConstraint(['id_data_center_structure'], ['data_center_structure.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_file_data_center_structure'),
        ForeignKeyConstraint(['id_file_type'], ['file_type.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_file_file_type'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_file_station'),
        PrimaryKeyConstraint('id', name='pk_files'),
        UniqueConstraint('md5checksum', name='uq_rinex_file_md5checksum'),
        Index('idx_file', 'id_station'),
        Index('idx_file_1', 'id_data_center_structure'),
        {'comment': 'List of RINEX data files.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    name = Column(Text, nullable=False, comment='Rinex file name.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table.')
    id_data_center_structure = Column(Integer, nullable=False, comment='Reference (foreign key) from data_center_structure table.')
    id_file_type = Column(Integer, nullable=False, comment='Reference (foreign key) from file_type table.')
    relative_path = Column(Text, nullable=False, comment='Relative path of the file directory.')
    reference_date = Column(DateTime, nullable=False, comment='Nominal start of data interval, format timestamp (YYYY-MM-DD hh:mm:ss).')
    creation_date = Column(DateTime, nullable=False, comment='The time when the file was created by the originating datacenter, format timestamp (YYYY-MM-DD hh:mm:ss).')
    published_date = Column(DateTime, nullable=False, comment='The time when the file is made publicly available on the server, format timestamp (YYYY-MM-DD hh:mm:ss).')
    revision_date = Column(DateTime, nullable=False, comment='The time the file contents was last changed, format timestamp (YYYY-MM-DD hh:mm:ss).')
    md5checksum = Column(Text, nullable=False, comment='File unique md5checksum value.')
    md5uncompressed = Column(Text, nullable=False, comment='Md5checksum value of the uncompressed file.')
    status = Column(SmallInteger, nullable=False, comment='Set to specific value to know if the file is available for dissemination or set to revision.')
    file_size = Column(Integer, comment='File size.')
    status_metadata = Column(SmallInteger, comment='Status based on check of the RINEX header information wrt station metadata, status defined by fwss')
    status_quality = Column(SmallInteger, comment='Status based on the quality of data, status defined by fwss')
    status_quarantine = Column(SmallInteger, comment='Status based on the downloadability of the file or other consideration, this status can be decided by the node manager or the DGW to stop the publication of a file without the need to remove it completely from the system')
    rinex_version = Column(Text, comment='RINEX files version(e.g. RINEX2.11 or RINEX3.04).')
    qc_published_date = Column(DateTime, comment='Quality control publishing date.')

    data_center_structure = relationship('DataCenterStructure', back_populates='rinex_file')
    file_type = relationship('FileType', back_populates='rinex_file')
    station = relationship('Station', back_populates='rinex_file')
    qc_report_summary = relationship('QcReportSummary', back_populates='rinex_file')
    rinex_errors = relationship('RinexErrors', back_populates='rinex_file')


class StationColocation(Base):
    __tablename__ = 'station_colocation'
    __table_args__ = (
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_colocation_stations_station'),
        ForeignKeyConstraint(['id_station_colocated'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_colocation_stations_station_colocated'),
        PrimaryKeyConstraint('id', name='pk_station_colocation'),
        Index('idx_station_colocation', 'id_station'),
        Index('idx_station_colocation_0', 'id_station_colocated'),
        {'comment': 'Colocated stations with measured offset.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='First colocated station.')
    id_station_colocated = Column(Integer, nullable=False, comment='Second colocated station.')

    station = relationship('Station', foreign_keys=[id_station], back_populates='station_colocation')
    station_ = relationship('Station', foreign_keys=[id_station_colocated], back_populates='station_colocation_')
    colocation_offset = relationship('ColocationOffset', back_populates='station_colocation')


class StationContact(Base):
    __tablename__ = 'station_contact'
    __table_args__ = (
        ForeignKeyConstraint(['id_contact'], ['contact.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_contact_id'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_id'),
        PrimaryKeyConstraint('id', name='pk_station_contact'),
        {'comment': 'List  with the contact person for each station.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table.')
    id_contact = Column(Integer, nullable=False, comment='Reference (foreign key) from contact table.')
    role = Column(Text, comment='Station contact person role.')

    contact = relationship('Contact', back_populates='station_contact')
    station = relationship('Station', back_populates='station_contact')


class StationItem(Base):
    __tablename__ = 'station_item'
    __table_args__ = (
        ForeignKeyConstraint(['id_item'], ['item.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_item_item'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_item_station'),
        PrimaryKeyConstraint('id', name='pk_station_item'),
        Index('idx_station_item_id_item', 'id_item'),
        Index('idx_station_item_id_station', 'id_station'),
        {'comment': 'List of items associated with a station.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign key) from station table.')
    id_item = Column(Integer, nullable=False, comment='Reference (foreign key) from item table.')
    date_from = Column(DateTime, nullable=False, comment='Date of item instalation in timestamp format (YYYY-MM-DD hh:mm:ss).')
    date_to = Column(DateTime, comment='Date of item removal in timestamp format (YYYY-MM-DD hh:mm:ss).')

    item = relationship('Item', back_populates='station_item')
    station = relationship('Station', back_populates='station_item')


class StationNetwork(Base):
    __tablename__ = 'station_network'
    __table_args__ = (
        ForeignKeyConstraint(['id_network'], ['network.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_network_id'),
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_station_id'),
        PrimaryKeyConstraint('id', name='pk_station_network'),
        {'comment': 'List of stations that belong to a network.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station = Column(Integer, nullable=False, comment='Reference (foreign_key) from station table.')
    id_network = Column(Integer, nullable=False, comment='Reference (foreign_key) from network table.')

    network = relationship('Network', back_populates='station_network')
    station = relationship('Station', back_populates='station_network')


class UserGroupStation(Base):
    __tablename__ = 'user_group_station'
    __table_args__ = (
        ForeignKeyConstraint(['id_station'], ['station.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_user_group_station_stations'),
        ForeignKeyConstraint(['id_user_group'], ['user_group.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_user_group_station_user_group'),
        PrimaryKeyConstraint('id', name='pk_user_group_station'),
        Index('idx_user_group_station', 'id_station'),
        Index('idx_user_group_station_0', 'id_user_group'),
        {'comment': 'Access control bridge table (many-to-many) connecting user groups '
                'and stations.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_user_group = Column(Integer, comment='Reference (foreign_key) from user_group table.')
    id_station = Column(Integer, comment='Reference (foreign_key) from station table.')

    station = relationship('Station', back_populates='user_group_station')
    user_group = relationship('UserGroup', back_populates='user_group_station')


class ColocationOffset(Base):
    __tablename__ = 'colocation_offset'
    __table_args__ = (
        ForeignKeyConstraint(['id_station_colocation'], ['station_colocation.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_colocation_offset'),
        PrimaryKeyConstraint('id', name='pk_colocation_offset'),
        Index('idx_colocation_offset', 'id_station_colocation'),
        {'comment': 'Measured offset of first colocated station to second colocated '
                'station. Contains history, most recent entry is current.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_station_colocation = Column(Integer, nullable=False, comment='Reference (foreign key) from station_colocation.')
    date_measured = Column(Date, nullable=False, comment='Offset measurement date.')
    offset_x = Column(Numeric, comment='Measured colocation x-axis offset in meters.')
    offset_y = Column(Numeric, comment='Measured colocation y-axis offset in meters.')
    offset_z = Column(Numeric, comment='Measured colocation z-axis offset in meters.')

    station_colocation = relationship('StationColocation', back_populates='colocation_offset')


class HighrateReportSummary(Base):
    __tablename__ = 'highrate_report_summary'
    __table_args__ = (
        ForeignKeyConstraint(['id_rinex_file_highrate'], ['highrate_rinex_file.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_highrate_report_summary'),
        PrimaryKeyConstraint('id', name='pk_highrate_report_summary'),
        {'comment': 'Contains summary of information contains in the RINEX header and '
                'observations total values over all constellations.'}
    )

    id = Column(BigInteger, primary_key=True)
    date_beg = Column(DateTime, nullable=False)
    date_end = Column(DateTime, nullable=False)
    data_smp = Column(Float(53), nullable=False)
    file_format = Column(Text, nullable=False)
    id_rinex_file_highrate = Column(Integer)
    created_timestamp = Column(DateTime)
    data_numb_epo = Column(Integer)
    site_id = Column(Text)
    marker_name = Column(Text)
    marker_numb = Column(Text)
    receiver_type = Column(Text)
    receiver_numb = Column(Text)
    receiver_vers = Column(Text)
    antenna_dome = Column(Text)
    antenna_numb = Column(Text)
    antenna_type = Column(Text)
    software = Column(Text)
    data_int = Column(Text)
    position_x = Column(Text)
    position_y = Column(Text)
    position_z = Column(Text)
    eccentricity_e = Column(Text)
    eccentricity_n = Column(Text)
    eccentricity_u = Column(Text)
    satellite_system = Column(Text, comment='From RINEX header SYS / # / OBS TYPES first char (example: “GRE” or “GPS+GLO+GAL” to be defined)')

    highrate_rinex_file = relationship('HighrateRinexFile', back_populates='highrate_report_summary')


class HighrateRinexErrors(Base):
    __tablename__ = 'highrate_rinex_errors'
    __table_args__ = (
        ForeignKeyConstraint(['id_error_type'], ['highrate_rinex_error_types.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_errors_highrate_error_type'),
        ForeignKeyConstraint(['id_rinex_file_highrate'], ['highrate_rinex_file.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_errors_highrate'),
        PrimaryKeyConstraint('id', name='pk_rinex_errors_highrate')
    )

    id = Column(Integer, primary_key=True)
    id_rinex_file_highrate = Column(Integer)
    id_error_type = Column(Integer)

    highrate_rinex_error_types = relationship('HighrateRinexErrorTypes', back_populates='highrate_rinex_errors')
    highrate_rinex_file = relationship('HighrateRinexFile', back_populates='highrate_rinex_errors')


class QcReportSummary(Base):
    __tablename__ = 'qc_report_summary'
    __table_args__ = (
        ForeignKeyConstraint(['id_qc_parameters'], ['qc_parameters.id'], name='fk_qc_report_qc_parameters'),
        ForeignKeyConstraint(['id_rinexfile'], ['rinex_file.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_report_file'),
        PrimaryKeyConstraint('id', name='pk_qc_summary'),
        Index('idx_qc_filesum', 'id_rinexfile'),
        Index('idx_qc_filesum_0', 'id_qc_parameters'),
        {'comment': 'Contains summary for the QC file, total values over all '
                'constellations.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_qc_parameters = Column(Integer, nullable=False, comment='Reference (foreign key) from qc_parameters table.')
    date_beg = Column(DateTime, nullable=False, comment='The first time epoch of data from any GNSS system, date timestamp format (YYYY-MM-DD hh:mm:ss).')
    date_end = Column(DateTime, nullable=False, comment='The last time epoch of data from any GNSS system, date timestamp format (YYYY-MM-DD hh:mm:ss).')
    data_smp = Column(Float(53), nullable=False, comment='Estimated data sampling interval [s].')
    obs_elev = Column(Float, nullable=False, comment='Minimum observation elevation angle.')
    obs_have = Column(Integer, nullable=False, comment='Sum of existing phase obs (all GNSS, band/frequencies).')
    user_have = Column(Integer, nullable=False, comment='User_have value.')
    cyc_slps = Column(Integer, nullable=False, comment='Count of phase obs cycle-slips.')
    xbeg = Column(Integer, nullable=False, comment='Excluded epochs before begin.')
    xend = Column(Integer, nullable=False, comment='Excluded epochs after nominal end.')
    xint = Column(Integer, nullable=False, comment='Excluded epochs in nominal sampling.')
    xsys = Column(Integer, nullable=False, comment='Excluded data from extra systems.')
    file_format = Column(Text, nullable=False, comment='File_format from RINEX, information comes from <head>/<file_format> in Anubis xml output.')
    id_rinexfile = Column(Integer, comment='Reference (foreign key) from rinex_file table.')
    obs_expt = Column(Integer, comment='Sum of expected phase obs (all GNSS, band/frequencies).')
    user_expt = Column(Integer, comment='User_expt value.')
    clk_jmps = Column(Integer, comment='Count of receiver clock jumps.')
    created_timestamp = Column(DateTime, comment='<meta><created> in Anubis xml output.')
    data_numb_epo = Column(Integer, comment='Number of epochs in the file.')
    site_id = Column(Text, comment='Site name taken from <head><site_id> in Anubis xml output')
    marker_name = Column(Text, comment='Marker name taken from <head><marker_name> in Anubis xml output')
    marker_numb = Column(Text, comment='Marker number taken from <head><marker_number> in Anubis xml output')
    receiver_type = Column(Text, comment='Receiver type taken from <head><receiver_type> in Anubis xml output')
    receiver_numb = Column(Text, comment='Receiver number taken from <head><receiver_numb> in Anubis xml output')
    receiver_vers = Column(Text, comment='Receiver version taken from <head><receiver_vers> in Anubis xml output.')
    antenna_dome = Column(Text, comment='Antenna radome taken from <head><antenna_dome> in Anubis xml output')
    antenna_numb = Column(Text, comment='Antenna serial number   taken from <head><antenna_numb> in Anubis xml output')
    antenna_type = Column(Text, comment='Antenna type taken from <head><antenna_type> in Anubis xml output')
    software = Column(Text, comment='Software taken from <head><software> in Anubis xml output.')
    data_int = Column(Text, comment='data interval taken from <head><data_int> in Anubis xml output')
    position_x = Column(Text, comment=' Position_X taken from <head><position><coordinate><gml:Point><gml:pos>[0] in Anubis xml output')
    position_y = Column(Text, comment='Position_Y taken from <head><position><coordinate><gml:Point><gml:pos>[1] in Anubis xml output')
    position_z = Column(Text, comment='Position_Z taken from <head><position><coordinate><gml:Point><gml:pos>[2] in Anubis xml output')
    eccentricity_e = Column(Text, comment='East component of eccentricity taken from <head><eccentricity><axis name="E"> in Anubis xml output')
    eccentricity_n = Column(Text, comment='North component of eccentricity taken from <head><eccentricity><axis name="N"> in Anubis xml output')
    eccentricity_u = Column(Text, comment='Up component of eccentricity taken from <head><eccentricity><axis name="U"> in Anubis xml output')

    qc_parameters = relationship('QcParameters', back_populates='qc_report_summary')
    rinex_file = relationship('RinexFile', back_populates='qc_report_summary')
    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='qc_report_summary')
    qc_navigation_msg = relationship('QcNavigationMsg', back_populates='qc_report_summary')


class RinexErrors(Base):
    __tablename__ = 'rinex_errors'
    __table_args__ = (
        ForeignKeyConstraint(['id_error_type'], ['rinex_error_types.id'], name='fk_rinex_errors_rinex_error_types'),
        ForeignKeyConstraint(['id_rinex_file'], ['rinex_file.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_rinex_errors_rinex_file'),
        PrimaryKeyConstraint('id', name='pk_rinex_errors'),
        Index('idx_rinex_errors', 'id_rinex_file'),
        Index('idx_rinex_errors_0', 'id_error_type'),
        {'comment': 'To identify rinex files with errors.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_rinex_file = Column(Integer, comment='Reference (foreign key) from rinex_file table.')
    id_error_type = Column(Integer, comment='Reference (foreign key) from error_type table.')

    rinex_error_types = relationship('RinexErrorTypes', back_populates='rinex_errors')
    rinex_file = relationship('RinexFile', back_populates='rinex_errors')


class QcConstellationSummary(Base):
    __tablename__ = 'qc_constellation_summary'
    __table_args__ = (
        ForeignKeyConstraint(['id_constellation'], ['constellation.id'], name='fk_qc_constellation_summary_constellation'),
        ForeignKeyConstraint(['id_qc_report_summary'], ['qc_report_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_constellation_summary_qc_report_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_summary_0'),
        Index('idx_qc_constellation_summary_id_constellation', 'id_constellation'),
        Index('idx_qc_summary', 'id_qc_report_summary'),
        {'comment': 'Contains a summary of the main metrics depending on the '
                'constellation.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_qc_report_summary = Column(Integer, nullable=False, comment='Reference (foreign key) from qc_report_summary table.')
    id_constellation = Column(Integer, nullable=False, comment='Reference (foreign key) from constellation table.')
    nsat = Column(Integer, nullable=False, comment='Total number of satellites.')
    xele = Column(Integer, nullable=False, comment='Satellites/epochs without elevation data.')
    epo_expt = Column(Integer, nullable=False, comment='Estimated number of expected data epochs.')
    epo_have = Column(Integer, nullable=False, comment='Estimated number of present data epochs.')
    epo_usbl = Column(Integer, nullable=False, comment='Estimated number of usable data epochs.')
    xpha_epo = Column(Integer, nullable=False, comment='Count of epochs with single-freq GNSS phase only.')
    xpha_sat = Column(Integer, nullable=False, comment='Count of epochs/sats with single-freq GNSS phase only.')
    xint_epo = Column(Integer, nullable=False, comment='Count of phase interruptions due to gap in epochs.')
    xint_sat = Column(Integer, nullable=False, comment='Count of phase interruptions due to gap in satellites.')
    xint_sig = Column(Integer, nullable=False, comment='Count of phase interruptions due to gap in signals.')
    xint_slp = Column(Integer, nullable=False, comment='Count of phase interruptions due to cycle-slip.')
    xcod_epo = Column(Integer, comment='Count of epochs with single-freq GNSS code only.')
    xcod_sat = Column(Integer, comment='Count of epochs/sats with single-freq GNSS code only.')
    x_crd = Column(Numeric(35, 15), comment='X_crd value.')
    y_crd = Column(Numeric(35, 15), comment='Y_crd value.')
    z_crd = Column(Numeric(35, 15), comment='Z_crd value.')
    x_rms = Column(Numeric(35, 15), comment='X_rms.')
    y_rms = Column(Numeric(35, 15), comment='Y_rms.')
    z_rms = Column(Numeric(35, 15), comment='Z_rms.')

    constellation = relationship('Constellation', back_populates='qc_constellation_summary')
    qc_report_summary = relationship('QcReportSummary', back_populates='qc_constellation_summary')
    qc_observation_summary_c = relationship('QcObservationSummaryC', back_populates='qc_constellation_summary')
    qc_observation_summary_d = relationship('QcObservationSummaryD', back_populates='qc_constellation_summary')
    qc_observation_summary_l = relationship('QcObservationSummaryL', back_populates='qc_constellation_summary')
    qc_observation_summary_s = relationship('QcObservationSummaryS', back_populates='qc_constellation_summary')


class QcNavigationMsg(Base):
    __tablename__ = 'qc_navigation_msg'
    __table_args__ = (
        ForeignKeyConstraint(['id_constellation'], ['constellation.id'], name='fk_qc_navigation_msg_constellation'),
        ForeignKeyConstraint(['id_qc_report_summary'], ['qc_report_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_navigation_msg_qc_report_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_navigation_msg_id'),
        Index('idx_qc_navigation_msg_id_constellation', 'id_constellation'),
        Index('idx_qc_navigation_msg_id_qc_report_summary', 'id_qc_report_summary'),
        {'comment': 'Navigation message numbers for each constellation.'}
    )

    id = Column(Integer, primary_key=True, comment='Autoincrement primary key.')
    id_qc_report_summary = Column(Integer, nullable=False, comment='Reference (foreign key) from qc_report_summary table.')
    id_constellation = Column(Integer, nullable=False, comment='Satellite system.')
    nsat = Column(Integer, nullable=False, comment='Number of satellites.')
    have = Column(Integer, nullable=False, comment='Number of existing messages.')

    constellation = relationship('Constellation', back_populates='qc_navigation_msg')
    qc_report_summary = relationship('QcReportSummary', back_populates='qc_navigation_msg')


class QcObservationSummaryC(Base):
    __tablename__ = 'qc_observation_summary_c'
    __table_args__ = (
        ForeignKeyConstraint(['id_gnss_obsnames'], ['gnss_obsnames.id'], name='fk_qc_observation_summary_c_gnss_obsnames'),
        ForeignKeyConstraint(['id_qc_constellation_summary'], ['qc_constellation_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_observation_summary_c_qc_constellation_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_observation_summary_c'),
        Index('idx_qc_observation_summary_c_id_gnss_obsnames', 'id_gnss_obsnames'),
        Index('idx_qc_observation_summary_c_id_qc_constellation_summary', 'id_qc_constellation_summary'),
        {'comment': 'Details about each observable of type C (pseudorange aka code) or '
                'type P (RINEX 2) of one constellation.'}
    )

    id = Column(BigInteger, primary_key=True, comment='Autoincrement primary key.')
    id_qc_constellation_summary = Column(Integer, nullable=False, comment='Link to the constellation summary for this QC file.')
    id_gnss_obsnames = Column(SmallInteger, nullable=False, comment='Link to the observation name (3 character following RINEX 3 observable naming convention).')
    obs_sats = Column(SmallInteger, nullable=False, comment='Number of satellites tracked on this channel.')
    obs_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal.')
    user_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal above the defined elevation cut off.')
    obs_expt = Column(Integer, comment='Sum of expected observations for this signal.')
    user_expt = Column(Integer, comment='Sum of expected observations for this signal above the defined elevation cut off.')
    cod_mpth = Column(Float(53), comment='Mean estimated code obs multipath and noise [cm].')

    gnss_obsnames = relationship('GnssObsnames', back_populates='qc_observation_summary_c')
    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='qc_observation_summary_c')


class QcObservationSummaryD(Base):
    __tablename__ = 'qc_observation_summary_d'
    __table_args__ = (
        ForeignKeyConstraint(['id_gnss_obsnames'], ['gnss_obsnames.id'], name='fk_qc_observation_summary_d_gnss_obsnames'),
        ForeignKeyConstraint(['id_qc_constellation_summary'], ['qc_constellation_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_observation_summary_d_qc_constellation_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_observation_summary_d'),
        Index('idx_qc_observation_summary_d_id_gnss_obsnames', 'id_gnss_obsnames'),
        Index('idx_qc_observation_summary_d_id_qc_constellation_summary', 'id_qc_constellation_summary'),
        {'comment': 'Details about each observable of type D (doppler) of one '
                'constellation.'}
    )

    id = Column(BigInteger, primary_key=True, comment='Autoincrement primary key.')
    id_qc_constellation_summary = Column(Integer, nullable=False, comment='Link to the constellation summary for this QC file.')
    id_gnss_obsnames = Column(SmallInteger, nullable=False, comment='Link to the observation name (3 character following RINEX 3 observable naming convention).')
    obs_sats = Column(SmallInteger, nullable=False, comment='Number of satellites tracked on this channel.')
    obs_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal.')
    user_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal above the defined elevation cut off.')
    obs_expt = Column(Integer, comment='Sum of expected observations for this signal.')
    user_expt = Column(Integer, comment='Sum of expected observations for this signal above the defined elevation cut off.')

    gnss_obsnames = relationship('GnssObsnames', back_populates='qc_observation_summary_d')
    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='qc_observation_summary_d')


class QcObservationSummaryL(Base):
    __tablename__ = 'qc_observation_summary_l'
    __table_args__ = (
        ForeignKeyConstraint(['id_gnss_obsnames'], ['gnss_obsnames.id'], name='fk_qc_observation_summary_l_gnss_obsnames'),
        ForeignKeyConstraint(['id_qc_constellation_summary'], ['qc_constellation_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_observation_summary_l_qc_constellation_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_observation_summary_l'),
        Index('idx_qc_observation_summary_l_id_gnss_obsnames', 'id_gnss_obsnames'),
        Index('idx_qc_observation_summary_l_id_qc_constellation_summary', 'id_qc_constellation_summary'),
        {'comment': 'Details about each observable of type L (carrier phase) of one '
                'constellation.'}
    )

    id = Column(BigInteger, primary_key=True, comment='Autoincrement primary key.')
    id_qc_constellation_summary = Column(Integer, nullable=False, comment='Link to the constellation summary for this QC file.')
    id_gnss_obsnames = Column(SmallInteger, nullable=False, comment='Link to the observation name (3 character following RINEX 3 observable naming convention).')
    obs_sats = Column(SmallInteger, nullable=False, comment='Number of satellites tracked on this channel.')
    obs_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal.')
    user_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal above the defined elevation cut off.')
    pha_slps = Column(Integer, nullable=False, comment='Counts of phase obs cycle slips.')
    obs_expt = Column(Integer, comment='Sum of expected observations for this signal.')
    user_expt = Column(Integer, comment='Sum of expected observations for this signal above the defined elevation cut off.')

    gnss_obsnames = relationship('GnssObsnames', back_populates='qc_observation_summary_l')
    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='qc_observation_summary_l')


class QcObservationSummaryS(Base):
    __tablename__ = 'qc_observation_summary_s'
    __table_args__ = (
        ForeignKeyConstraint(['id_gnss_obsnames'], ['gnss_obsnames.id'], name='fk_qc_observation_summary_s_gnss_obsnames'),
        ForeignKeyConstraint(['id_qc_constellation_summary'], ['qc_constellation_summary.id'], ondelete='CASCADE', onupdate='CASCADE', name='fk_qc_observation_summary_s_qc_constellation_summary'),
        PrimaryKeyConstraint('id', name='pk_qc_observation_summary_s'),
        Index('idx_qc_observation_summary_s_id_gnss_obsnames', 'id_gnss_obsnames'),
        Index('idx_qc_observation_summary_s_id_qc_constellation_summary', 'id_qc_constellation_summary'),
        {'comment': 'Details about each observable of type S (signal strength) of one '
                'constellation.'}
    )

    id = Column(BigInteger, primary_key=True, comment='Autoincrement primary key.')
    id_qc_constellation_summary = Column(Integer, nullable=False, comment='Link to the constellation summary for this QC file.')
    id_gnss_obsnames = Column(SmallInteger, nullable=False, comment='Link to the observation name (3 character following RINEX 3 observable naming convention).')
    obs_sats = Column(SmallInteger, nullable=False, comment='Number of satellites tracked on this channel.')
    obs_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal.')
    user_have = Column(Integer, nullable=False, comment='Sum of existing observations for this signal above the defined elevation cut off.')
    obs_expt = Column(Integer, comment='Sum of expected observations for this signal.')
    user_expt = Column(Integer, comment='Sum of expected observations for this signal above the defined elevation cut off.')

    gnss_obsnames = relationship('GnssObsnames', back_populates='qc_observation_summary_s')
    qc_constellation_summary = relationship('QcConstellationSummary', back_populates='qc_observation_summary_s')
